#!user/bin/tclsh

proc linear {a b y} {
    if {$a==0 && $b==$y} {
        puts "X c R"
    }
    if {$a==0&&$b==0&&$y!=0} {
        puts "X c 0"
    }
    if {$a==0&&$b!=$y} {
        puts "X c 0"
    }
    puts [expr ($y-$b)/$a]
}

if {![catch {exec /usr/bin/tty -s}]} {
    puts "Input number of tests"
}
gets stdin number
set i 0
while {$i<$number} {
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input A"
    }
    gets stdin a
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input B"
    }
    gets stdin b
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input C"
    }
    gets stdin c 
    linear $a $b $c 
    incr i
}
