#!user/bin/tclsh

proc gcd {a b} {
    if {$b==0} {return $a} else {gcd $b [expr {$a%$b}]}
}

if {![catch {exec /usr/bin/tty -s}]} {
    puts "Input number of tests"
}
gets stdin number
set i 0
while {$i<$number} {
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input A"
    }
    gets stdin a
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input B"
    }
    gets stdin b
    puts [gcd $a $b]
    incr i
}
