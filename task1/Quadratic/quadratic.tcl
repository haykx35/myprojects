#!user/bin/tclsh

proc linear {a b y} {
    if {$a==0 && $b==$y} {
        puts "X c R"
        return
    }
    if {$a==0&&$b==0&&$y!=0} {
        puts "X c 0"
        return
    }
    if {$a==0&&$b!=$y} {
        puts "X c 0"
        return
    }
    puts [expr ($y-$b)/$a]
}

proc discriminate {a b c y} {
       return [expr $b*$b-4*$a*($c - $y)]
}

proc quadratic {a b c y} {
    if {$a==0} {
        linear $b $c $y
        return 
    }
    set d [discriminate $a $b $c $y]

    if {$d==0} {
        puts [expr -$b/(2*$a)]
    }
    if {$d>0} {
        puts [expr (-$b + (pow($d, 0.5)))/(2*$a)]
        puts [expr (-$b - (pow($d, 0.5)))/(2*$a)]
        return
    }
    if {$d<0} {
        puts "Discriminate lees than null"
        return
    }
}

if {![catch {exec /usr/bin/tty -s}]} {
    puts "Input number of tests"
}
gets stdin number
set i 0
while {$i<$number} {
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input A"
    }
    gets stdin a
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input B"
    }
    gets stdin b
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input C"
    }
    gets stdin c 
    if {![catch {exec /usr/bin/tty -s}]} {
        puts "Input Y"
    }
    gets stdin y 
    quadratic $a $b $c $y
    incr i
}
