#include "headers/MiniComputer.hpp"

#include <iostream>

int
main()
{
    /// Create and run the Mini Computer
    MiniComputer mini;
    return mini.run();
}

