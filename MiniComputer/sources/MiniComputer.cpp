#include "headers/MiniComputer.hpp"


#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <cassert>

MiniComputer::MiniComputer()
{
    setComputerName("Mini");
    setVersion("1.0");
    registers_.resize(REGISTER_COUNT);
}

MiniComputer::MiniComputer(std::string name, std::string version)
{
    setComputerName(name);
    setVersion(version);
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

std::string
MiniComputer::getVersion()
{
    return version_;
}

void
MiniComputer::setVersion(std::string version)
{
    version_ = version;
}

int
MiniComputer::getRegisterValue(const int index)
{
    return registers_.at(index);
}

void
MiniComputer::printComputerNameAndVersion()
{
    std::cout << getComputerName() << ' ' << getVersion() << std::endl;
}

void
MiniComputer::printMenu(Menu menu)
{
    if (::isatty(STDIN_FILENO)) {
        static const std::string header[MENU_SIZE] = { "Main Menu", "Print Menu" };
        static const int sizes[MENU_SIZE] = { 5, 2 };
        static const std::string menuItems[MENU_SIZE][5] = {
            { "Exit", "Load", "Store", "Print", "Add" },
            { "Register", "String" }
        };
        std::cout << header[menu] << ":\n";
        for (int i = 0; i < sizes[menu]; ++i) {
            printMenuItem(menuItems[menu], i);
        }
    }
}

void
MiniComputer::printMenuItem(const std::string menuItems[], const int index)
{
    std::cout << '\t' << menuItems[index] << ": " << index << '\n';
}

int
MiniComputer::getCommandFromUser()
{
    int result;
    std::cin >> result;
    return result;
}

void
MiniComputer::validateCommand(int command)
{
    if (command < 0) {
        std::cerr << "Error 1: invalid command" << std::endl;
        ::exit(1);
    }
    if (command > 4) {
        std::cerr << "Error 1: invalid command" << std::endl;
        ::exit(1);
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exitting..." << std::endl;
}

void
MiniComputer::executeLoadCommand()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::executePrintRegister()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::executePrintString()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::validatePrint(int isStr)
{
    if (isStr < 0) {
        std::cerr << "Error 2: invalid print command" << std::endl;
        ::exit(2);
    }
    if (isStr > 1) {
        std::cerr << "Error 2: invalid print command" << std::endl;
        ::exit(2);
    }
}
void
MiniComputer::executePrintCommand()
{
    printMenu(PRINT_MENU);
    int isStr = getCommandFromUser();
    validatePrint(isStr);
    (1 == isStr) ? executePrintString() : executePrintRegister();
}

const bool isInteractive = ::isatty(STDIN_FILENO);

inline
int
getRegister(const std::string& prompt)
{
    if (::isInteractive) {
        std::cout << prompt;
    }

    int registerNumber;
    std::cin >> registerNumber;
    return registerNumber;
}

void
MiniComputer::executeAddCommand()
{
    /// printAddMenu();

    const int register1Number = getRegister("Register 1: ");
    const int register2Number = getRegister("Register 2: ");
    const int register3Number = getRegister("Register 3: ");

    std::cout << "Fake Result: "
              << (register1Number + register2Number + register3Number)
              << std::endl;
}

void
MiniComputer::executeCommand(int command)
{
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 3: executePrintCommand(); break;
    case 4: executeAddCommand();   break;
    default:
        assert(0);
        break;
    }
}

int
MiniComputer::run()
{
    /// Print program name and version
    printComputerNameAndVersion();

    int command = -1;
    do {
        /// Print main menu
        printMenu(MAIN_MENU);

        /// Get command from user
        command = getCommandFromUser();
        validateCommand(command);

        /// Execute the command
        executeCommand(command);
    } while (command != 0); /// Start again from main menu

    return 0;
}

