#ifndef __MINI_COMPUTER_HPP__
#define __MINI_COMPUTER_HPP__

#include "headers/IntVector.hpp"

#include <string>

const int REGISTER_COUNT = 4;

class MiniComputer
{
private:
    enum Menu { MAIN_MENU, PRINT_MENU, MENU_SIZE };

public:
    MiniComputer();
    MiniComputer(std::string name, std::string version);

    std::string getComputerName();
    void setComputerName(std::string name);

    std::string getVersion();
    void setVersion(std::string name);

    int run();

private:
    int getRegisterValue(const int index);
    void printComputerNameAndVersion();
    void printMenu(Menu menu);
    void printMenuItem(const std::string menuItems[], const int index);
    int getCommandFromUser();
    void executeCommand(int command);
    void executeExitCommand();
    void executeLoadCommand();
    void executePrintCommand();
    void executeAddCommand();
    void executePrintString();
    void executePrintRegister();
    void validateCommand(int command);
    void validatePrint(int command);

private:
    std::string computerName_;
    std::string version_;
    co03::IntVector registers_;
};

#endif /// __MINI_COMPUTER_HPP__

