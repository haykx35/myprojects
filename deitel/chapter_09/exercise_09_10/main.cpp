#include "Time.hpp"
#include <unistd.h>
#include <iostream>

int
main()
{
    if (::isatty(STDIN_FILENO)) { 
        std::cout << "Input hour, minute, second." << std::endl;
    }
    int hour, minute, second;
    std::cin >> hour >> minute >> second;

    Time time2(hour, minute, second);
    switch (time2.setTime(hour, minute, second)) {
    case 1: std::cout << "Error 1: Invalid hour."   << std::endl;   break;
    case 2: std::cout << "Error 2: Invalid minute." << std::endl;   break;
    case 3: std::cout << "Error 3: Invalid second." << std::endl;   break;
    }
    return 0;
}
