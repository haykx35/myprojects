#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time(const int hour = 0, const int minute = 0, const int second = 0); 
    int setTime (const int hour, const int minute, const int second);
    int setHour(const int hour); 
    int setMinute(const int minute);
    int setSecond(const int second);

    int getHour() const;
    int getMinute() const; 
    int getSecond() const; 

    void printUniversal(); 
    void printStandard(); 
private:
    int hour_;
    int minute_; 
    int second_; 
};

#endif /// __TIME_HPP__
