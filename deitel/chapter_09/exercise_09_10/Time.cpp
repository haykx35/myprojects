#include "Time.hpp"
#include <iomanip>
#include <iostream>

Time::Time(const int hour, const int minute, const int second )
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
} 

int
Time::setTime(const int hour, const int minute, const int second)
{
    const int hourErrorNumber = setHour(hour);
    if (hourErrorNumber != 0) {
        return hourErrorNumber;
    }
    
    const int minuteErrorNumber = setMinute(minute);
    if (minuteErrorNumber != 0) {
        return minuteErrorNumber;
    }
    const int secondErrorNumber = setSecond(second);
    if (secondErrorNumber != 0) {
        return secondErrorNumber;
    }
    return 0;
} 

int 
Time::setHour(const int hour)
{
    if (hour < 0 || hour > 24) {
        return 1;
    }
    hour_ = hour;
    return 0;
} 

int
Time::setMinute(const int minute)
{
    if (minute < 0 || minute > 59) {
        return 2;
    }
    minute_ = minute;
    return 0;
} 

int 
Time::setSecond(int second)
{
    if (second < 0 || second > 59) {
        return 3;
    } 
    second_ = second;
    return 0;
}

int 
Time::getHour() const
{
    return hour_;
} 
int 
Time::getMinute() const
{
    return minute_;
}

int 
Time::getSecond() const 
{
    return second_;
}

void 
Time::printUniversal()
{
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":" 
                                   << std::setw(2) << getMinute () << ":" 
                                   << std::setw(2) << getSecond () << std::endl;
    std::cout << std::setfill(' ');
} 

void 
Time::printStandard()
{
    std::cout << ((0 == getHour() || 12 == getHour()) ? 12 : getHour() % 12) 
              << ":" <<  std::setfill('0') << std::setw(2) << getMinute() 
              << ":" << std::setw(2) << getSecond() << (getHour() < 12 ? " AM" : " PM") << std::endl;
    std::cout << std::setfill(' ');
}

