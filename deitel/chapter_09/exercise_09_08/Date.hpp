#ifndef __DATE_HPP__
#define __DATE_HPP__

class Date
{
public:
    Date(const int day = 1, const int month = 1, const int year = 1970);
    void setTime(const int day, const int month, const int year);
    void setDay(const int day);
    void setMonth(const int month);
    void setYear(const int year);
    int getDay() const;
    int getMonth() const;
    int getYear() const;
    void print();
    void nextDay();
private:
    int day_;
    int month_;
    int year_;
};

#endif /// __DATE_HPP__
