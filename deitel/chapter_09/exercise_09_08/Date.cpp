#include "Date.hpp"
#include <iostream>

Date::Date(const int day, const int month, const int year)
{
    setTime(day, month, year);
}

void
Date::setTime(const int day, const int month, const int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

inline
void
Date::setDay(const int day)
{
    day_ = (day >= 1 && day < 31) ? day : 1;
}

inline
void
Date::setMonth(const int month)
{
    month_ = (month >= 1 && month < 13) ? month : 1;
}

inline
void
Date::setYear(const int year)
{
    year_ = year;
}

inline
int
Date::getDay() const
{
    return day_;
}

inline
int
Date::getMonth() const
{
    return month_;
}

inline
int
Date::getYear() const
{
    return year_;
}

void
Date::print()
{
    std::cout << getDay() << "/"
              << getMonth() << "/"
              << getYear() << std::endl;
}

void
Date::nextDay()
{
    setDay(getDay() + 1);
    if (1 == getDay()) {
        setMonth(getMonth() + 1);
        if (1 == getMonth()) {
            setYear(getYear() + 1);
        }
    }
}

