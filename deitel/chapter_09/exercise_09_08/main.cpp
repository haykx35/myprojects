#include "Date.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    Date firstDay;
    firstDay.print();

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input day, month and year." << std::endl;
    }
    int year, month, day;
    std::cin >> day >> month >> year;

    Date dayOne(day, month, year);
    for (int i = 0; i < 10; ++i) {
        dayOne.nextDay();
        dayOne.print();
    }
    return 0;
}
