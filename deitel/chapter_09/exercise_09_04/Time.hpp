
#ifndef __TIME_HPP__
#define __TIME_HPP__

#include <ctime>

class Time
{
public:
    Time();
    void setTime(const int hour, const int minute, const int second);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    int getHour();
    int getMinute();
    int getSecond();
    void print();
private:
    int hour_;
    int minute_;
    int second_;
};

#endif /// __TIME_HPP__
