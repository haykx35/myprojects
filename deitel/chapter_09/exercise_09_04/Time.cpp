#include "exercise_09_04.hpp"
#include <iostream>
#include <iomanip>
#include <ctime>
#include <cassert>

Time::Time()
{ 
    const time_t currentTime = ::time(NULL);
    const int second = static_cast<int>(currentTime) % 60;
    const time_t currentTime1 = currentTime / 60;
    const int minute = static_cast<int>(currentTime1) % 60;
    const time_t currentTime2 = currentTime1 / 60;
    const int hour = static_cast<int>(currentTime2) % 24 + 4;
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    assert(hour >= 0 && hour < 24);
    hour_ = hour;
}

void
Time::setMinute(const int minute)
{
    assert(minute >= 0 && minute < 60);
    minute_ = minute;
}

void
Time::setSecond(const int second)
{
    assert(second >= 0 && second < 60);
    second_ = second;
}

int
Time::getHour()
{
    return hour_;
}

int
Time::getMinute()
{
    return minute_;
}

int
Time::getSecond()
{
    return second_;
}

void
Time::print()
{
    std::cout << "Current time is ";
    std::cout << std::setfill('0') << std::setw(2) << hour_ << ':'
                                   << std::setw(2) << minute_ << ':'
                                   << std::setw(2) << second_ << std::endl;
    std::cout << std::setfill(' ');
}

