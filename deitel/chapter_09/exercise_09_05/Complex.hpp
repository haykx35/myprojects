#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex(const double realPart = 0, const double imaginaryPart = 0);

    Complex plus(const Complex& rhv); 
    Complex minus(const Complex& rhv); 
    void print();
    void setRealPart(const double realPart);
    void setImaginaryPart(const double imaginaryPart);
private:
    double realPart_;
    double imaginaryPart_;
};

#endif /// __COMPLEX__HPP__

