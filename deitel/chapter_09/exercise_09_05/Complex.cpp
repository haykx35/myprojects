#include "Complex.hpp"
#include <iostream>

Complex::Complex(const double realPart, const double imaginaryPart)
{
    setRealPart(realPart);
    setImaginaryPart(imaginaryPart);
}

void
Complex::setRealPart(const double realPart)
{
    realPart_ = realPart;
}

void
Complex::setImaginaryPart(const double imaginaryPart)
{
    imaginaryPart_ = imaginaryPart;
}

Complex
Complex::plus(const Complex& rhv)
{
    return Complex(realPart_ + rhv.realPart_, imaginaryPart_ + rhv.imaginaryPart_);
}
Complex
Complex::minus(const Complex& rhv)
{
    return Complex(realPart_ - rhv.realPart_, imaginaryPart_ - rhv.imaginaryPart_);
}
void
Complex::print()
{
    std::cout << "(" << realPart_ << " + " << imaginaryPart_ << "i" << ")" << std::endl;
}

