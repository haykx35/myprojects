#include "Complex.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two complex numbers: "; 
    }
    double real1, real2, imaginary1, imaginary2;
    std::cin >> real1 >> imaginary1 >> real2 >> imaginary2;
    Complex c1(real1, imaginary1), c2(real2, imaginary2);
    Complex c3 = c1.plus(c2);
    c3.print();
    Complex c4 = c3.minus(c2);
    c4.print();
    return 0; 
}
