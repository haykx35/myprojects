#include "Board.hpp"
#include "Player.hpp"
#include "TicTacToe.hpp"
#include <iostream>

TicTacToe::TicTacToe(Board* b1, Player* p1, Player* p2) 
{
    b_  = b1;
    p1_ = p1;
    p2_ = p2;
}

int
TicTacToe::play()
{
    b_->print();
    while (!b_->hasFinished()) {
        if (!b_->checkWin()) {
            std::cout << "Player 1: " << std::endl;
            const int coordinate = p1_->getCordFromUser();
            p1_->setOnBoard(b_, coordinate); 
            b_->print();
        }
        
        if (b_->checkWin()) {
            std::cout << "Player 1 Won:" << std::endl << "CONGRATULATIONS" << std::endl;
            break;
        }

        if (!b_->checkWin()) {
            std::cout << "Player 2: " << std::endl;
            int coordinate = p2_->getCordFromUser();
            p2_->setOnBoard(b_, coordinate);
            b_->print();
        }
        
        if (b_->checkWin()) {
            std::cout << "Player 2 Won:" << std::endl << "CONGRATULATIONS" << std::endl;
            break;
        }
    }

    if (b_->hasFinished()) {
        std::cout << "STANDOFF" << std::endl;
    }
    return 0;
}


