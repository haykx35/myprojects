#include "Board.hpp"
#include <iostream>
#include <cstdlib>
#include <string>

Board::Board()  
{
    char coordinate = '1'; 
    for (int i = 0; i < BOARD_SIZE; ++i) {
       for (int j = 0; j < BOARD_SIZE; ++j, ++coordinate) {
           board_[i][j] = coordinate;
        }
    }
}

void
Board::print() const
{
    ::system("clear");
    for (int i = 0; i < BOARD_SIZE; ++i) {
        for (int j = 0; j < BOARD_SIZE; ++j) {
            std::cout  << '|' << board_[i][j] << '|';
        }
        std::cout << std::endl;
    }
}

void
Board::mark(const int coordinate, const char symbol)
{
    const int x = getXCoordinate(coordinate);
    const int y = getYCoordinate(coordinate);
    if (!isOccupied(coordinate)) { 
        board_[x][y] = symbol;
    }
}

bool
Board::hasFinished() const
{
    for (int i = 1; i < BOARD_SIZE; ++i) {
        for (int j = 1; j < BOARD_SIZE; ++j) {
            if (board_[i][j] != 'X' || board_[i][j] != 'O') {
                return false;
            }
        }
    }
    return true;
}

bool
Board::checkWin() const
{
    Board::isRowWin();
    Board::isColumnWin();
    Board::isDiagonalWin();
    return false;
}

Board*
Board::getBoard()
{
    return &(*this);
}

bool 
Board::isOccupied(const int coordinate) const
{
    const int x =  getXCoordinate(coordinate);
    const int y =  getYCoordinate(coordinate);
    if (board_[x][y] == 'X' || board_[x][y] == 'O') {
        return true;
    }
    return false;
}

bool
Board::isRowWin() const
{
    if (board_[0][0] == board_[0][1] && board_[0][1] == board_[0][2] && (board_[0][2] == 'X' || board_[0][2] == 'O')) {
        return true;
    }
    if (board_[1][0] == board_[1][1] && board_[1][1] == board_[1][2] && (board_[1][2] == 'X' || board_[1][2] == 'O')) {
        return true;
    }
    if (board_[2][1] == board_[2][1] && board_[2][1] == board_[2][2] && (board_[2][2] == 'X' || board_[2][2] == 'O')) {
        return true;
    }
    return false;
}

bool
Board::isColumnWin() const
{
    if (board_[0][0] == board_[1][0] && board_[1][0] == board_[2][0] && (board_[2][0] == 'X' || board_[2][0] == 'O')) {
        return true;
    }
    if (board_[0][1] == board_[1][1] && board_[1][1] == board_[2][1] && (board_[2][1] == 'X' || board_[2][1] == 'O')) {
        return true;
    }
    if (board_[0][2] == board_[1][2] && board_[1][2] == board_[2][2] && (board_[2][2] == 'X' || board_[2][2] == 'O')) {
        return true;
    }
    return false;
}

bool
Board::isDiagonalWin() const
{
    if (board_[0][0] == board_[1][1] && board_[1][1] == board_[2][2] && (board_[2][2] == 'X' || board_[2][2] == 'O')) {
        return true;
    }
    if (board_[0][2] == board_[1][1] && board_[1][1] == board_[2][0] && (board_[2][0] == 'X' || board_[2][0] == 'O')) {
        return true;
    }
    return false;

}

int 
Board::getXCoordinate(const int coordinate) const
{
    const int x = coordinate / BOARD_SIZE;
    return x;
}

int 
Board::getYCoordinate(const int coordinate) const
{
    const int y = coordinate % BOARD_SIZE;
    return y;
}
