#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__
class Player
{
public:
    Player(Board* b1, const char symbol);
    int getCordFromUser() const;
    Board* getBoard(Board* b1) const;
    void setOnBoard(Board* b1, const int coordinate);
private:
    char symbol_;
};
#endif /// __PLAYER_HPP__
