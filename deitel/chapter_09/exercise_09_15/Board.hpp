#ifndef __BOARD_HPP__
#define __BOARD_HPP__

class Board
{
private:
    static const int BOARD_SIZE = 3;
public:
    Board();
    void print() const;
    void mark(const int coordinate, const char symbol);
    bool hasFinished() const;
    bool checkWin() const;
    bool isOccupied(const int coordinate) const;
    Board* getBoard();
private:
    bool isRowWin() const;
    bool isColumnWin() const;
    bool isDiagonalWin() const;
    int getXCoordinate(const int coordinate) const;
    int getYCoordinate(const int coordinate) const;
private:
    char board_[BOARD_SIZE][BOARD_SIZE];
};
#endif /// __BOARD_HPP__
