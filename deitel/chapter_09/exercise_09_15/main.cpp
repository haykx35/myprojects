#include "TicTacToe.hpp"
#include "Board.hpp"
#include "Player.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    Board b;
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Player 1 choose your symbol X or O: ";
    }
    char p1symbol;
    std::cin >> p1symbol;
    if (p1symbol != 'O' && p1symbol !='X') { 
        std::cerr << "Error 1: Invalid symbol " << p1symbol << std::endl;
        return 1;
    }
    char p2symbol;
    if(p1symbol == 'X') {
        p2symbol = 'O';
    }
    if(p1symbol == 'O') {
        p2symbol = 'X';
    }
    Player p1(&b, p1symbol);
    Player p2(&b, p2symbol);
    TicTacToe t(&b, &p1, &p2);
    return t.play();
}

