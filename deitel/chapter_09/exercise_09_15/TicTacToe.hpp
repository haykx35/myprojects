#ifndef __TIC_TAC_TOE_HPP__
#define __TIC_TAC_TOE_HPP__

#include "Board.hpp"
#include "Player.hpp"

class TicTacToe
{
public:
    TicTacToe(Board* b, Player* p1, Player* p2);
public:
    int play();
private:
    Player* p1_;
    Player* p2_;
    Board* b_;
};
#endif /// __TIC_TAC_TOE_HPP__
