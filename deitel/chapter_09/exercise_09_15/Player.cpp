#include "Board.hpp"
#include "Player.hpp"
#include <iostream>
#include <unistd.h>
#include <cassert>

Player::Player(Board* b1, const char symbol)
    : symbol_(symbol)
{
    getBoard(b1);
}

void
Player::setOnBoard(Board* b1, const int coordinate)
{
    b1->mark(coordinate - 1, symbol_);
}

int
Player::getCordFromUser()
{
    int coordinate;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input coordinate:";
    }
    std::cin >> coordinate;
    assert(coordinate > 0 || coordinate < 9);
    return coordinate;
}

Board*
Player::getBoard(Board* b1) const
{
    return b1->getBoard();
}
