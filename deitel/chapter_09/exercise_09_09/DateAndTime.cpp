#include "DateAndTime.hpp"
#include <iomanip>
#include <iostream>

DateAndTime::DateAndTime(const int day, const int month, const int year, const int hour, const int minute, const int second)
{
    setDateAndTime(day, month, year, hour, minute, second);
}

void
DateAndTime::setDateAndTime(const int day, const int month, const int year, const int hour, const int minute, const int second)
{
    setDay(day);
    setMonth(month);
    setHour(hour); 
    setMinute(minute);
    setSecond(second);
    setYear(year);
}

void 
DateAndTime::setHour(const int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
} 

void 
DateAndTime::setMinute(const int minute)
{
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
} 

void 
DateAndTime::setSecond(const int second)
{
    second_ = (second >= 0  && second < 60) ? second : 0;
}

void
DateAndTime::setDay(const int day)
{
    day_ = (day >= 1 && day < 31) ? day : 1;
}

void
DateAndTime::setMonth(const int month)
{
    month_ = (month >= 1 && month < 13) ? month : 1;
}

void
DateAndTime::setYear(const int year)
{
    year_ = year;
}

int
DateAndTime::getDay() 
{
    return day_;
}

int
DateAndTime::getMonth()
{
    return month_;
}

int
DateAndTime::getYear() 
{
    return year_;
}

int 
DateAndTime::getHour()
{
    return hour_;
} 

int 
DateAndTime::getMinute() 
{
    return minute_;
}

int 
DateAndTime::getSecond() 
{
    return second_;
}

void
DateAndTime::print()
{
    std::cout << std::setfill('0') << std::setw(2) << getSecond() << ":" 
                                   << std::setw(2) << getMinute () << ":" 
                                   << std::setw(2) << getHour() << '\t'
                                   << std::setw(2) << getDay() << ":"
                                   << std::setw(2) << getMonth() << ":"
                                   << getYear() << std::endl;
    std::cout << std::setfill(' ');

}

void
DateAndTime::tick()
{
    setSecond(getSecond() + 1);
    if (0 == getSecond()) {
        setMinute(getMinute() + 1);
        if (0 == getMinute()) {
            setHour(getHour() + 1);
            if (0 == getHour()) {
                nextDay();
            }
        }
    } 
    
}

void
DateAndTime::nextDay()
{
    setDay(getDay() + 1);
    if (1 == getDay()) {
        setMonth(getMonth() + 1);
        if (1 == getMonth()) {
            setYear(getYear() + 1);
        }
    }
}

