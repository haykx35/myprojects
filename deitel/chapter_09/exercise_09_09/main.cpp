#include "DateAndTime.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    DateAndTime myDay;
    myDay.print();;
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "input day, month, year, hour, minute, second." << std::endl; 
    }
    int day, month, year, hour, minute, second;
    std::cin >> day >> month >> year >> hour >> minute >> second;

    DateAndTime newYear(day, month, year, hour, minute, second);
    for (int i = 0; i < 10; ++i) {
        newYear.tick();
        newYear.print();
    }
    return 0;
}
