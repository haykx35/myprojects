#ifndef __DATEANDTIME_HPP__
#define __DATEANDTIME_HPP__

class DateAndTime
{
public:
    DateAndTime(const int day = 1, const int month = 1, const int year = 1970, const int hour = 0, const int minute = 0, const int second = 0);
    void setDateAndTime(const int day, const int month, const int year, const int hour, const int minute, const int second);
    void setDay(const int day);
    void setMonth(const int month);
    void setHour(const int hour); 
    void setMinute(const int minute);
    void setSecond(const int second);
    void setYear(const int year);

    int getDay();
    int getMonth();
    int getHour();
    int getMinute(); 
    int getSecond(); 
    int getYear();

    void print(); 
    void tick();
    void nextDay();
private:
    int hour_;
    int minute_; 
    int second_; 
    int day_;
    int month_;
    int year_;
 
};

#endif /// __DATEANDTIME_HPP__

