#include "Time.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input hour, minute and second." << std::endl;
    }
    int hour, minute, second;
    std::cin >> hour >> minute >> second;
    
    Time time01(hour, minute, second);
    time01.printUniversal();
    time01.printStandard();
    
    for (int i = 0; i < 10; ++i) {
        time01.tick();
        time01.printUniversal();
        time01.printStandard();
    }
    return 0;
}

