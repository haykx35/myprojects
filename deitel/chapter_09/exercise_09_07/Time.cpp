#include "Time.hpp" 
#include <iostream>
#include <iomanip>

Time::Time(const int hour, const int minute, const int second )
{
    setTime (hour, minute, second); 
} 

void 
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour); 
    setMinute(minute);
    setSecond(second);
} 

void 
Time::setHour(const int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
} 

void 
Time::setMinute(const int minute)
{
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
} 

void 
Time::setSecond(const int second)
{
    second_ = (second >= 0 && second < 60) ? second : 0;
}

int 
Time::getHour() const
{
    return hour_;
} 

int 
Time::getMinute() const
{
    return minute_;
}

int 
Time::getSecond() const 
{
    return second_;
}

void 
Time::printUniversal()
{
    std::cout << std::setfill('0') << std::setw( 2 ) << getHour() << ":" 
                                   << std::setw( 2 ) << getMinute () << ":" 
                                   << std::setw( 2 ) << getSecond () << std::endl;
    std::cout << std::setfill(' ');
} 

void 
Time::printStandard()
{
    std::cout << ((0 == getHour() || 12 == getHour()) ? 12 : getHour() % 12) 
              << ":" <<  std::setfill('0') << std::setw(2) << getMinute() 
              << ":" << std::setw(2) << getSecond() << (getHour() < 12 ? " AM" : " PM") << std::endl;
    std::cout << std::setfill(' ');
}

void
Time::tick()
{
    setSecond(getSecond() + 1);
    if (0 == getSecond()) {
        setMinute(getMinute() + 1);
        if (0 == getMinute()) {
            setHour(getHour() + 1);
        }
    } 
}

