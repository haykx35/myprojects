#include "Point.hpp"
#include <cassert>

Point::Point(const double x, const double y) 
{ 
    setPointX(x);
    setPointY(y);
}

void 
Point::setPointX(const double x)
{
    assert(x > 0.0 && x < 20.0);
    x_ = x;
}

void
Point::setPointY(const double y)
{
    assert(y > 0.0 && y < 20.0);
    y_ = y;
}

