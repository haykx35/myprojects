#include "Rectangle.hpp"
#include "Point.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

Rectangle::Rectangle(const Point& a, const Point& b, const Point& c, const Point& d)
{
    setPoints(a, b, c, d);
}


void
Rectangle::setPoints(const Point& a, const Point& b, const Point& c, const Point& d) 
{
    assert(isRectangle(a, b, c, d));
    setPointA(a);
    setPointB(b);
    setPointC(c);
    setPointD(d);
}

void
Rectangle::setPointA(const Point& a)
{
    a_ = a;
}

void
Rectangle::setPointB(const Point& b)
{
    b_ = b;
}

void
Rectangle::setPointC(const Point& c)
{
    c_ = c;
}

void
Rectangle::setPointD(const Point& d)
{
    d_ = d;
}

const Point&
Rectangle::getPointA() const
{
    return a_;
}

const Point&
Rectangle::getPointB() const
{
    return b_;
}

const Point&
Rectangle::getPointC() const 
{
    return c_;
}

const Point&
Rectangle::getPointD() const
{
    return d_;
}

bool
Rectangle::isRectangle(const Point& a, const Point& b, const Point& c, const Point& d)
{
    return (Rectangle::distance(a, b) == Rectangle::distance(c, d) && Rectangle::distance(a, c) == Rectangle::distance(b, d) && Rectangle::distance(a, d) == Rectangle::distance(b, c));
}

double
Rectangle::distance(const Point& a, const Point& b)
{
    const int deltaX = (a.x_ - b.x_);
    const int deltaY = (a.y_ - b.y_);
    return std::sqrt(deltaX * deltaX + deltaY * deltaY);
}

double
Rectangle::length() const
{ 
    const double distanceAB = distance(getPointA(), getPointB());
    const double distanceBC = distance(getPointB(), getPointC());

    const double distanceAC = distance(getPointA(), getPointC());
    if ((distanceAB > distanceBC && distanceAB < distanceAC) || (distanceAB > distanceAC && distanceAB < distanceBC)) {
        return distanceAB;
    }
    if ((distanceBC > distanceAB && distanceBC < distanceAC) || (distanceBC > distanceAC && distanceBC < distanceAB)) {
        return distanceBC;
    }
    if ((distanceAC > distanceBC && distanceAC < distanceAB) || (distanceAC > distanceAB && distanceAC < distanceBC)) {
        return distanceAC;
    }
    return distanceAC;   
}

double
Rectangle::width() const
{   

    double minNumber = distance(getPointA(), getPointB());
    const double distanceBC = distance(getPointB(), getPointC());
    if (minNumber > distanceBC) {
        minNumber = distanceBC;
    }
    const double distanceAD = distance(getPointA(), getPointD());
    if (minNumber > distanceAD) {
        minNumber = distanceAD;
    }
    return minNumber;
}

double
Rectangle::perimeter() const
{
    return (length() + width()) * 2;
}

double
Rectangle::area() const
{
    return length() * width(); 
}

bool
Rectangle::isSquare()
{
    return length() == width();
}

double
Rectangle::inputCoordinate()
{
    double coordinate;
    std::cin >> coordinate;
    return coordinate;
}

bool
Rectangle::validCoordinate(const double coordinate)
{
    return (coordinate > 0.0 && coordinate < 20.0);
}

void
Rectangle::printComment()
{
    std::cerr << "Error 1: Invalid coordinate." << std::endl;
}

