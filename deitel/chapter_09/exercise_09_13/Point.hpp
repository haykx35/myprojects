#ifndef __POINT_HPP__
#define __POINT_HPP__
#include <cassert>

struct Point
{
public:
    Point(const double x = 1.0, const double y = 1.0); 
    void setPointX(const double x);
    void setPointY(const double y);

    double x_;
    double y_;
};

#endif /// __POINT_HPP__
