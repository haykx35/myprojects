#include "Point.hpp"
#include "Rectangle.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter coordinates." << std::endl;
    }

    const double coordinateX1 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateX1))) {
        Rectangle::printComment();
        return 1;
    }
    const double coordinateY1 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateY1))) {
        Rectangle::printComment();
        return 1;
    }
    const Point a(coordinateX1, coordinateY1);


    const double coordinateX2 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateX2))) {
        Rectangle::printComment();
        return 1;
    }
    const double coordinateY2 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateY2))) {
        Rectangle::printComment();
        return 1;
    }
    const Point b(coordinateX2, coordinateY2);


    const double coordinateX3 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateX3))) {
        Rectangle::printComment();
        return 1;
    }
    const double coordinateY3 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateY3))) {
        Rectangle::printComment();
        return 1;
    }


    const Point c(coordinateX3, coordinateY3);
    const double coordinateX4 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateX4))) {
        Rectangle::printComment();
        return 1;
    }
    const double coordinateY4 = Rectangle::inputCoordinate();
    if (!(Rectangle::validCoordinate(coordinateY4))) {
        Rectangle::printComment();
        return 1;
    }
    const Point d(coordinateX4, coordinateY4);


    if (!(Rectangle::isRectangle(a, b, c, d))) {
        std::cerr << "Error 2: It is not Rectangle." << std::endl;
        return 2;
    }
    Rectangle myRectangle(a, b, c, d);

    std::cout << "The length is " << myRectangle.length() <<  std::endl;

    std::cout << "The width is " << myRectangle.width() << std::endl;

    std::cout << "The area is " << myRectangle.area() << std::endl;

    std::cout << "The perimeter is " << myRectangle.perimeter() << std::endl;

    if (myRectangle.isSquare()) {
        std::cout << "It is square." << std::endl;
        return 0;
    } 
    std::cout << "It is not square." << std::endl;
    return 0;
}

