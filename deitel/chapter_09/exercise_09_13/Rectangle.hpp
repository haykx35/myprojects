#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

#include "Point.hpp"

class Rectangle
{
public:
    static void printComment();
    static double inputCoordinate();
    static double distance(const Point& a, const Point& b);
    static bool isRectangle(const Point& a, const Point& b, const Point& c, const Point& d);
    static bool validCoordinate(const double coordinate);
public:
    Rectangle(const Point& a, const Point& b, const Point& c, const Point& d);
    void setPoints(const Point& a, const Point& b, const Point& c, const Point& d);
    void setPointA(const Point& a);
    void setPointB(const Point& b);
    void setPointC(const Point& c);
    void setPointD(const Point& d);
    const Point& getPointA() const;
    const Point& getPointB() const;
    const Point& getPointC() const;
    const Point& getPointD() const;
    double length() const;
    double width() const;
    double perimeter() const;
    double area() const;
    bool isSquare();
private:
    Point a_;
    Point b_;
    Point c_;
    Point d_;
};

#endif /// __RECTANGLE_HPP__
