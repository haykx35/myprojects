#include "Rectangle.hpp"
#include <cassert>
#include <iostream>

Rectangle::Rectangle(const double length, const double width)
{
    setSides(length, width);
}

void
Rectangle::setSides(const double length, const double width)
{
    setLength(length);
    setWidth(width);
}

void
Rectangle::setLength(const double length)
{
    assert(length > 0.0 && length < 20.0);
    length_ = length;
}

void
Rectangle::setWidth(const double width)
{
    assert(width > 0.0 && width < 20.0);
    width_ = width;
}

double
Rectangle::getLength()
{
    return length_;
}

double
Rectangle::getWidth()
{
    return width_;
}

double
Rectangle::perimeter()
{
    const double perimeter = 2 * (getLength() + getWidth());
    return perimeter;
}

double
Rectangle::area()
{
    const double area = getLength() * getWidth();
    return area;
}

