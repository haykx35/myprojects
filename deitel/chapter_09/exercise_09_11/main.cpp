#include "Rectangle.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Enter length and width." << std::endl;
    }
    double length, width;
    std::cin >> length >> width;

    if (length <= 0.0 || length >= 20.0) {
        std::cerr << "Error 1: Invalid value for length." << std::endl;
        return 1;
    }
    if (width <= 0.0 || length >= 20.0) {
        std::cerr << "Error 2: Invalid value for width." << std::endl;
        return 2;
    }

    Rectangle myRectangle;
    std::cout << "Area is " << myRectangle.area() << std::endl;
    std::cout << "Perimeter is " << myRectangle.perimeter() << std::endl;

    Rectangle rectangle1(length, width);
    std::cout << "Area is " << rectangle1.area() << std::endl;
    std::cout << "Perimeter is " << rectangle1.perimeter() << std::endl;
    return 0;
}
