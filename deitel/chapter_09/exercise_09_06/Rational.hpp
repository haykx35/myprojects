#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational
{
public:
    Rational(const int numerator = 0, const int denominator = 1);
    void setNumerator(const int numerator);
    void setDenominator(const int denominator);
    int getNumerator() const;
    int getDenominator() const;
    void print();
    void printDoubleFormat();
    Rational add(const Rational& rhv);
    Rational sub(const Rational& rhv);
    Rational multiply(const Rational& rhv);
    Rational divide(const Rational& rhv);

private:
    void reduce(const int reduceNumber);
    int gcd(int secondNumber, int firstNumber);
private:
    int numerator_;
    int denominator_;
};

#endif /// __RATIONAL_HPP__
