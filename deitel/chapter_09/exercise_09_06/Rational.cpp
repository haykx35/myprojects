#include "Rational.hpp"
#include <cassert>
#include <iostream>
#include <cstdlib>

Rational::Rational(const int numerator, const int denominator)
{
    setNumerator(numerator);
    setDenominator(denominator);
    const int reduceNumber = gcd(numerator, denominator);
    reduce(reduceNumber);
}

void
Rational::setNumerator(const int numerator)
{
    numerator_ = numerator;
}

void
Rational::setDenominator(const int denominator)
{
    assert(denominator != 0);
    denominator_ = denominator;
}

int
Rational::getNumerator() const 
{
    return numerator_;
}

int
Rational::getDenominator() const
{
    return denominator_;
}

void
Rational::reduce(const int reduceNumber)
{
    numerator_ /= reduceNumber;
    denominator_ /= reduceNumber;
}

int
Rational::gcd(int secondNumber, int firstNumber)
{
    if (secondNumber > firstNumber) {
        int insteadNumber = secondNumber;
        secondNumber = firstNumber;
        firstNumber = insteadNumber;
    }
    if (0 == secondNumber) {
        return firstNumber;
    }
    return gcd(secondNumber, firstNumber % secondNumber);
}

Rational
Rational::add(const Rational& rhv)
{
    const int numerator3 = rhv.getDenominator() * getNumerator() + rhv.getNumerator() * getDenominator();
    const int denumerator3 = getDenominator() * rhv.getDenominator();
    Rational number3(numerator3, denumerator3);
    return number3;
}

Rational
Rational::sub(const Rational& rhv)
{
    const int numerator3 = rhv.getDenominator() * getNumerator() - rhv.getNumerator() * getDenominator();
    const int denumerator3 = getDenominator() * rhv.getDenominator();
    Rational number3(numerator3, denumerator3);
    return number3;
}

Rational
Rational::multiply(const Rational& rhv)
{
    const int numerator3 = rhv.getNumerator() * getNumerator();
    const int denumerator3 = getDenominator() * rhv.getDenominator();
    Rational number3(numerator3, denumerator3);
    return number3;
}

Rational
Rational::divide(const Rational& rhv)
{
    assert(0 != rhv.getNumerator());
    const int numerator3 = rhv.getDenominator() * getNumerator();
    const int denumerator3 = getDenominator() * rhv.getNumerator();
    Rational number3(numerator3, denumerator3);
    return number3;
}

void
Rational::print()
{
    std::cout << getNumerator() << " / " << getDenominator() << std::endl;
}

void
Rational::printDoubleFormat()
{
    const double doubleResult = static_cast<double>(getNumerator()) / static_cast<double>(getDenominator());
    std::cout << doubleResult << std::endl;
}

