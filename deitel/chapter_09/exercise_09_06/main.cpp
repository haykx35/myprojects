#include "Rational.hpp"
#include <unistd.h>
#include <iostream>

int
main()
{
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Enter numerators and denumerators: "; 
    }
    int numerator1, denumerator1;
    std::cin >> numerator1 >> denumerator1;

    int numerator2, denumerator2;
    std::cin >> numerator2 >> denumerator2;
    if (0 == denumerator1 || 0 == denumerator2) {
        std::cerr << "Error 1: Invalid denumerators." << std::endl;
        return 1;
    }
    Rational number1(numerator1, denumerator1);
    Rational number2(numerator2, denumerator2);

    Rational number3 = number1.add(number2);
    number3.print();
    number3.printDoubleFormat();

    Rational number4 = number1.sub(number2);
    number4.print();
    number4.printDoubleFormat();

    Rational number5 = number1.multiply(number2);
    number5.print();
    number5.printDoubleFormat();

    if (0 == numerator2) {
        std::cerr << "Error 2: Invalid numerator." << std::endl;
        return 2;
    }
    Rational number6 = number1.divide(number2);
    number6.print();
    number6.printDoubleFormat();

    return 0;
}
