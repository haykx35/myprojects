#include "Package.hpp"
#include "OvernightPackage.hpp"
#include "TwoDaysPackage.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{   
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input Information: " << std::endl;
    }
    Person  sender;
    std::cin >> sender.name_;
    std::cin >> sender.address_;
    std::cin >> sender.city_;
    std::cin >> sender.state_;
    std::cin >> sender.zipCode_;

    Person  recipient;
    std::cin >> recipient.name_;
    std::cin >> recipient.address_;
    std::cin >> recipient.city_;
    std::cin >> recipient.state_;
    std::cin >> recipient.zipCode_;


    double weight;
    std::cin >> weight;
    double costPerOunce;
    std::cin >> costPerOunce;
    Package package1(weight, costPerOunce, sender, recipient);
    std::cout << "Our service costed " << package1.calculateCost() << std::endl;

    double flatFee;
    std::cin >> flatFee;
    TwoDayPackage package2(weight, costPerOunce, flatFee, sender, recipient);
    std::cout << "Our service costed " << package2.calculateCost() << std::endl;

    std::cin >> flatFee;
    OvernightPackage package3(weight, costPerOunce, flatFee, sender, recipient);
    std::cout << "Our service costed " << package3.calculateCost() << std::endl;
    return 0;
}

