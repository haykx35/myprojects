#ifndef __OVVERNIGHTPACKAGE_HPP__
#define __OVVERNIGHTPACKAGE_HPP__

#include "Package.hpp"

class OvernightPackage : public Package
{
public:
    OvernightPackage(const double weight, const double costPerOunce, const double flatFee, Person sender, Person recipient);
public:
    void setFlatFee(const double flatFee);
    double getFlatFee() const;
    double calculateCost();
private:
    double flatFee_;
};
#endif /// __OVERNIGHTPACKAGE_HPP__
