#ifndef __PACKAGE_HPP__
#define __PACKAGE_HPP__
#include <string>

struct Person
{
    std::string name_;
    std::string address_;
    std::string city_;
    std::string state_;
    std::string zipCode_;
    void print();
};

class Package
{
public:
    Package(const double weight, const double costPerOunce, Person sender, Person recipient);
public:
    double calculateCost();
    void setWeight(const double weight);
    void setCostPerOunce(const double costPerOunce);
    double getWeight() const;
    double getCostPerOunce() const;
private:
    Person sender_;
    Person recipient_;
    double weight_;
    double costPerOunce_;
};
#endif /// __PACKAGE_HPP__

