#include "TwoDaysPackage.hpp"
#include <string>
#include <cassert>
#include <iostream>

TwoDayPackage::TwoDayPackage(const double weight, const double costPerOunce, const double flatFee, Person sender, Person recipient)
    : Package(weight, costPerOunce, sender, recipient)
{
    setFlatFee(flatFee);
}

double
TwoDayPackage::calculateCost()
{
    return Package::calculateCost() + (Package::calculateCost() * getFlatFee());
}

void
TwoDayPackage::setFlatFee(const double flatFee)
{
    assert(flatFee > 0 || flatFee < 1);
    flatFee_ = flatFee;
}

double
TwoDayPackage::getFlatFee() const
{
    return flatFee_;
}

