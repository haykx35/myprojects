#include "OvernightPackage.hpp"
#include <iostream>
#include <cassert>

OvernightPackage::OvernightPackage(const double weight, const double costPerOunce, const double flatFee, Person sender, Person recipient)
    : Package(weight, costPerOunce, sender, recipient)
{
    setFlatFee(flatFee);
}

void
OvernightPackage::setFlatFee(const double flatFee)
{
    assert(flatFee >= 0 || flatFee <= 1);
    flatFee_ = flatFee;
}

double
OvernightPackage::getFlatFee() const
{
    return flatFee_;
}

double
OvernightPackage::calculateCost()
{
    return Package::calculateCost() * (1 + getFlatFee());
}

