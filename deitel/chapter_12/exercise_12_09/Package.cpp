#include "Package.hpp"
#include <iostream>
#include <string>
#include <cassert>

void
Person::print()
{
    std::cout << "Name: " << name_ << std::endl;
    std::cout << "Address: " << address_ << std::endl;
    std::cout << "City: " << city_ << std::endl;
    std::cout << "State: " << state_ << std::endl;
    std::cout << "Zip-code: " << zipCode_ << std::endl;
}

Package::Package(const double weight, const double costPerOunce, Person sender, Person recipient)
    :sender_(sender),
     recipient_(recipient)
{
    setWeight(weight);
    setCostPerOunce(costPerOunce);
    sender.print();
    std::cout << std::endl;
    recipient.print();
    std::cout << std::endl;
}

double 
Package::calculateCost()
{
    return getWeight() * getCostPerOunce();
}

void
Package::setWeight(const double weight)
{
    assert(weight >= 0);
    weight_ = weight;
}

void
Package::setCostPerOunce(const double costPerOunce)
{
    assert(costPerOunce >= 0);
    costPerOunce_ = costPerOunce;
}

double
Package::getCostPerOunce() const
{
    return costPerOunce_;
}

double 
Package::getWeight() const
{
    return weight_;
}

