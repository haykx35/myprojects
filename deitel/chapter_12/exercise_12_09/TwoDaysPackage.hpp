#ifndef __TWODAYSPACKAGE_HPP__
#define __TWODAYSPACKAGE_HPP__
#include "Package.hpp"
#include <string>


class TwoDayPackage : public Package
{
public:
    TwoDayPackage(const double weight, const double costPerOunce, const double flatFee, Person sender, Person recipient);
    double calculateCost();
    void setFlatFee(const double flatFee);
    double getFlatFee() const;
private:
    double flatFee_;
};
#endif /// __TWODAYSPACKAGE_HPP__
