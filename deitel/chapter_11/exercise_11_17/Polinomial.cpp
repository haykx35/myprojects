#include "Polinomial.hpp"
#include <iostream>
#include <cassert>

std::ostream&
operator<<(std::ostream& output, const Polinomial& rhv)
{
    for (int i = 0; i < rhv.degree_; ++i) {
        if (0 == rhv.coef_[i]) continue;
        if (i > 0 && rhv.coef_[i] >= 0) {
            output << '+';
        }
        output << rhv.coef_[i]; 
        output << "x^";
        output << i;
    }
    return output;
}

Polinomial::Polinomial()
    : degree_(0)
    , coef_(NULL)
{
}

Polinomial::Polinomial(const int* coef, const int degree)
    : degree_(degree)
    , coef_(new int[degree])
{
    assert(degree > 0);
    for (int i = 0; i < degree_; ++i) {
        coef_[i] = coef[i];
    }
}

Polinomial::Polinomial(const Polinomial& rhv)
    : degree_(rhv.degree_)
    , coef_(new int[rhv.degree_])
{
    for (int i = 0; i < degree_; ++i) {
        coef_[i] = rhv.coef_[i];
    }
}

Polinomial::~Polinomial()
{
    if (coef_ != NULL) {
        delete [] coef_;
        coef_ = NULL;
    }

}

Polinomial&
Polinomial::setCoef(int* coef)
{
    coef_ = coef;
    return *this;
}

Polinomial&
Polinomial::setDegree(const int degree)
{
    degree_ = degree;
    return *this;
}

int
Polinomial::getCoef(const int exponent) const
{
    if (exponent < 0 || exponent >= degree_) {
        return 0;
    }
    return coef_[exponent];
}

Polinomial
Polinomial::operator+(const Polinomial& rhv) const
{
    const int size = std::max(degree_, rhv.degree_);
    int* coef = new int[size];
    for (int i = 0; i < size; ++i) {
        coef[i] = getCoef(i) + rhv.getCoef(i);
    }
 
    return Polinomial().setCoef(coef).setDegree(size);
}
   
Polinomial
Polinomial::operator-(const Polinomial& rhv) const
{
    const int degree = std::max(degree_, rhv.degree_);
    int* coef = new int[degree];
    for (int i = 0; i < degree; ++i) {
        coef[i] = getCoef(i) - rhv.getCoef(i);
    }
 
    return Polinomial().setCoef(coef).setDegree(degree);
}

Polinomial
Polinomial::operator*(const Polinomial& rhv) const
{
    const int degree = degree_ + rhv.degree_;
    int* coef = new int[degree];
    for (int i = 0; i < degree_; ++i) {
        for (int j = 0; j < rhv.degree_; ++j) {
            coef[i + j] += getCoef(i) * rhv.getCoef(j);
        }
    }
    return Polinomial().setCoef(coef).setDegree(degree);
}

   
const Polinomial&
Polinomial::operator=(const Polinomial& rhv)
{
    if (this == &rhv) {
        return *this;
    }

    if (degree_ != rhv.degree_) {
        delete [] coef_;
        degree_ = rhv.degree_;
        coef_ = new int [degree_];
    }

    for (int i = 0; i < degree_; ++i) {
        coef_[i] = rhv.getCoef(i);
    }
    
    return *this;
}

std::ostream&
operator<<(std::ostream& output, const Polinomial& rhv)
{
    for (int i = 0; i < rhv.degree_; ++i) {
        if (0 == rhv.coef_[i]) continue;
        if (i > 0 && rhv.coef_[i] >= 0) {
            output << '+';
        }
        output << rhv.coef_[i]; 
        output << "x^";
        output << i;
    }
    return output;
}

