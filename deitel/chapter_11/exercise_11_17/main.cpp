#include "Polinomial.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input size of your polinomial: ";
        std::cout << "Input coefficients of your polinomial starting from degree's: ";
    }
    int size1;
    std::cin >> size1;
    if (size1 <= 0) {
        std::cerr << "Error 1: Invalid size." << std::endl;
        return 1;
    }
    int* coef1 = new int[size1];
    for (int i = 0; i < size1; ++i) {
        std::cin >> coef1[i];
    }
    const Polinomial polinomial1(coef1, size1);
    std::cout << polinomial1 << std::endl;

    int size2;
    std::cin >> size2;
    if (size2 <= 0) {
        std::cerr << "Error 1: Invalid size" << std::endl;
        return 1;
    }
    int* coef2 = new int[size2];
    for (int i = 0; i < size2; ++i) {
        std::cin >> coef2[i];
    }
    const Polinomial polinomial2(coef2, size2);
    std::cout << polinomial2 << std::endl;
    
    const Polinomial polinomial3 = polinomial1 + polinomial2;
    std::cout << polinomial3 << std::endl;
    
    Polinomial polinomial4 = polinomial1 - polinomial2;
    std::cout << polinomial4 << std::endl;
    
    Polinomial polinomial5(polinomial4);
    std::cout << polinomial5 << std::endl;

    polinomial4 = polinomial5 * polinomial2;
    std::cout << polinomial4 << std::endl;
 
    return 0;
}
