#ifndef __POLINOMIAL_HPP__
#define __POLINOMIAL_HPP__

#include <iostream>

class Polinomial
{
    friend std::ostream& operator<<(std::ostream& output, const Polinomial& rhv);
public:
    Polinomial(const int* coef, const int degree);
    Polinomial(const Polinomial& rhv);
    ~Polinomial();
    const Polinomial& operator=(const Polinomial& rhv);
    Polinomial operator+(const Polinomial& rhv) const;
    Polinomial operator-(const Polinomial& rhv) const;
    Polinomial operator*(const Polinomial& rhv) const;
    int getCoef(const int exponent) const;
private:
    Polinomial();
    Polinomial& setCoef(int* coef);
    Polinomial& setDegree(const int degree);
private:
    int degree_;
    int* coef_;
};

#endif ///__POLINOMIAL_HPP__

