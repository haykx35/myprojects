#include <iostream>

int
main()
{
    const long value1 = 200000;
    long value2;
    const long* longPtr = NULL;

    longPtr = &value1;
    std::cout << *longPtr << std::endl;
    value2 = *longPtr;

    std::cout << value2 << std::endl;
    std::cout << &value1 << std::endl;
    std::cout << longPtr << std::endl;
    /// the value printed the same as value1'ss address
    return 0;
}
