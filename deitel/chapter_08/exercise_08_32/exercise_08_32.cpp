#include <iostream>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <ctime>

int
main()
{
    const int WORD_COUNT = 5;
    const int SENTENCE_LENGTH = 6;
    const int SENTENCE_COUNT = 20;

    const char* article[WORD_COUNT] = { "the", "a", "one", "some", "any"};
    const char* verb[WORD_COUNT] = { "drove", "jumped", "ran", "walked", "skipped"};
    const char* noun[WORD_COUNT] = { "boy", "girl", "dog", "town", "car"};
    const char* preposition[WORD_COUNT] = { "to", "from", "over", "under", "on"};

    const char* simbol = " ";    
    const char** sentence[SENTENCE_LENGTH] = { article, noun, verb, preposition, article, noun };
    
    if (::isatty(STDIN_FILENO)) {
        ::srand(::time(0));
    }
    for (int i = 0; i < SENTENCE_COUNT; ++i) {
        char array[80] = {}; 
        for (int j = 0; j < SENTENCE_LENGTH; ++j) {
            std::strcat(array, sentence[j][::rand() % WORD_COUNT]);
            std::strcat(array, simbol);
        }
        array[std::strlen(array) - 1] = '.';
        array[0] = std::toupper(array[0]);
        std::cout << array << std::endl;
    }
    return 0;
}

