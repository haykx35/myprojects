#include <iostream>
#include <cstring>
#include <unistd.h>

int
main()
{
    char array1[80];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input first string: " << std::endl;
    }
    std::cin.getline(array1, 80, '\n');
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second string: " << std::endl;
    }
    char array2[80];
    std::cin.getline(array2, 80, '\n');

    int compare = std::strcmp(array1, array2);
    std::cout << compare << std::endl; /// checking that compare is 0 , negative or nonnegative value
    if (compare < 0) {
        std::cout << array1 << " is less than " << array2 << std::endl;
        return 0;
    } 
    if (compare > 0) {
        std::cout << array1 << " is greater than " << array2 << std::endl;
        return 0;
    }
    std::cout << array1 << " and " << array2 << " are equal " << std::endl;
    return 0;
}
