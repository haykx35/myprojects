#include <iostream>
#include <cstdlib>
#include <string>
#include <cassert>
#include <unistd.h>

enum Classes {ALL_CLASSES, FIRST_CLASS, ECONOMY_CLASS, CLASS_COUNT};
enum Seats {FIRST_SEAT, LAST_SEAT};
const bool isInteractive = ::isatty(STDIN_FILENO);

void executeReservation(bool planeSeats[], const int seatCount);
void classIsChanged(const char select);
void printMenu();
void printAvailableSeats(const bool planeSeats[], const int firstSeat, const int lastSeat, const int seatClass);
void printBoardingPass(const int seatClass, const int seatNumber);
void printChangeClassMenu(const int seatClass);
void printError(const int errorNumber, const std::string& errorMessage);
int getSeatClass();
std::string getClassName(const int seatClass);
int getOppositeSeatClass(const int seatClass);
int getSeatNumber(const int firstSeat, const int lastSeat);
char getSelectionFromUser();
bool reserveSeat(bool planeSeats[], const int seatNumber);
bool seatsAreReserved(const bool planeSeats[], const int firstSeat, const int lastSeat);

int
main()
{
    const int SEAT_CAPACITY = 10;
    bool planeSeats[SEAT_CAPACITY] = {};
    executeReservation(planeSeats, SEAT_CAPACITY);
    std::cout << "Next flight leaves in 3 hours." << std::endl;

    return 0;
}

inline void
executeReservation(bool planeSeats[], const int seatCount)
{
    static const int SEAT_UNITS = 2;
    const int halfSeats = seatCount / 2;
    static const int seatRange[][SEAT_UNITS] = {
        { FIRST_SEAT, seatCount }, 
        { FIRST_SEAT, halfSeats },  
        { halfSeats,  seatCount }  
    };  

    while (!seatsAreReserved(planeSeats, seatRange[ALL_CLASSES][FIRST_SEAT], seatRange[ALL_CLASSES][LAST_SEAT])) {
        printMenu(); 
        const int seatClass = getSeatClass();
        assert (FIRST_CLASS == seatClass || ECONOMY_CLASS == seatClass); 
        if (!seatsAreReserved(planeSeats, seatRange[seatClass][FIRST_SEAT], seatRange[seatClass][LAST_SEAT])) {
            printAvailableSeats(planeSeats, seatRange[seatClass][FIRST_SEAT], seatRange[seatClass][LAST_SEAT], seatClass);
            const int seatNumber = getSeatNumber(seatRange[seatClass][FIRST_SEAT], seatRange[seatClass][LAST_SEAT]);
            if (reserveSeat(planeSeats, seatNumber)) {
                printBoardingPass(seatClass, seatNumber);
            }
        } else {
            printChangeClassMenu(seatClass);
            classIsChanged(getSelectionFromUser());
        }
    }
}

inline void
classIsChanged(const char select)
{
    switch (select) {
    case 'y': case 'Y':                                                             return;
    case 'n': case 'N': std::cout << "Next flight leaves in 3 hours." << std::endl; return;
    default: printError(2, "Wrong selection input!");
    }
}

inline void
printMenu()
{
    std::cout << "Please type 1 for \"First Class\":\n"
              << "Please type 2 for \"Economy\":" << std::endl;
}

inline void
printAvailableSeats(const bool planeSeats[], const int firstSeat, const int lastSeat, const int seatClass)
{
    static const int SHIFTER = 1; 
    std::cout << getClassName(seatClass) << " available seats:\n";
    for (int i = firstSeat; i < lastSeat; ++i) {
        if (!planeSeats[i]) {
            std::cout << "\tSeat No " << i + SHIFTER << "\n";
        }
    }
}

inline void
printBoardingPass(const int seatClass, const int seatNumber)
{
    std::cout << "CLASS\t\t"
              << getClassName(seatClass) << "\nSEAT NUMBER\t\t" << "___"
              << seatNumber  << std::endl;
}

inline void
printChangeClassMenu(const int seatClass)
{
    std::cout << "All " << getClassName(seatClass)
              << " seats are reserved.\n" << "Would you like to get a place in " 
              << getClassName(getOppositeSeatClass(seatClass)) << "? y/n: \n";
}

inline void
printError(const int errorNumber, const std::string& errorMessage)
{
    std::cerr << "Error " << errorNumber << ": " << errorMessage << std::endl;
    ::exit(errorNumber);
}

inline int
getSeatClass()
{
    int seatClass;
    std::cin >> seatClass;
    if (FIRST_CLASS != seatClass && ECONOMY_CLASS != seatClass) {
        printError(1, "Wrong menu number!");
    }
    return seatClass;
}

inline std::string
getClassName(const int seatClass)
{
    assert(FIRST_CLASS == seatClass || ECONOMY_CLASS == seatClass);
    if (FIRST_CLASS == seatClass) {
        return "\"First class\"";
    }
    return "\"Econom Class\"";
}

inline int
getOppositeSeatClass(const int seatClass)
{
    assert(FIRST_CLASS == seatClass || ECONOMY_CLASS == seatClass);
    return (FIRST_CLASS + ECONOMY_CLASS - seatClass);
}

inline int
getSeatNumber(const int firstSeat, const int lastSeat)
{
    if (::isInteractive) {
        std::cout << "Input seat number to reserve: ";
    }
    int seatNumber;
    std::cin >> seatNumber;
    if (seatNumber < firstSeat || seatNumber > lastSeat) {
        printError(3, "Wrong seat number!");
    }
    return seatNumber;
}

inline char
getSelectionFromUser()
{
    char select;
    std::cin >> select;
    return select;
}

inline bool
reserveSeat(bool planeSeats[], const int seatNumber)
{
    static const int SHIFTER = 1; 
    static const bool IS_RESERVED = true;
    if (planeSeats[seatNumber - SHIFTER]) {
        if (::isInteractive) {
            std::cout << "The seat is already reserved!" << std::endl;
            return false;
        }
    } else {
        planeSeats[seatNumber - SHIFTER] = IS_RESERVED;
    }
    return true;
}

inline bool
seatsAreReserved(const bool planeSeats[], const int firstSeat, const int lastSeat)
{
    for (int i = firstSeat; i < lastSeat; ++i) {
        if (!planeSeats[i]) {
            return false;
        }
    }
    return true;
}


