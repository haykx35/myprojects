#include <unistd.h>
#include <iostream>

int smallestElement(int array[], const int beginIndex, const int endIndex);
void inputNumber(int array[], const int size);

int
main()
{
    const int SIZE = 7;
    int array[SIZE] = {};
    inputNumber(array, SIZE);
    const int index = smallestElement(array, 0, SIZE  -1);
    std::cout << "The smallest element is " << array[index] << std::endl;
    return 0;
    
}

int
smallestElement(int array[], const int beginIndex, const int endIndex)
{
    if (beginIndex == endIndex) {
        return endIndex;
    }
    int minIndex = smallestElement(array, beginIndex + 1, endIndex);
    if (array[beginIndex] < array[minIndex]) {
        minIndex = beginIndex;
    }
    return minIndex;  
}

void 
inputNumber(int array[], const int size)
{
    for (int i = 0; i < size; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter numbers: ";
        }
        std::cin >> array[i];
    }
}

