#include <unistd.h>
#include <iostream>

void bubbleSort(int array[], const int arraySize);
void printSortedArray(int array[], const int arraySize);
void inputArray(int array[], const int arraySize);

int
main()
{
    const int arraySize = 10;
    int array[arraySize] = {};
    
    inputArray(array, arraySize);
    bubbleSort(array, arraySize);
    printSortedArray(array, arraySize);
    return 0;
}

void
inputArray(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter number: ";
        }
        std::cin >> array[i];
    }
}
void
bubbleSort(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        for (int j = 0; j < arraySize - 1; ++j) {
            if (array[(j + 1)] < array[j]) {
                int number1 = array[j + 1];
                array[j + 1] = array[j];
                array[j] = number1;
            }
        }
    }
}

void
printSortedArray(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++ i) {
        std::cout << array[i] << std::endl;
    }
}
