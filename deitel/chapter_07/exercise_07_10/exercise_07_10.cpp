#include <iostream>
#include <unistd.h>

void inputArray(int array[], const int ARRAY_SIZE);
void printArray(int array[], const int ARRAY_SIZE);

int
main()
{
    const int ARRAY_SIZE = 9;
    int array[ARRAY_SIZE] = {};
    inputArray(array, ARRAY_SIZE);
    printArray(array, ARRAY_SIZE);
    return 0;
}

void
inputArray(int array[], const int ARRAY_SIZE)
{
    for (int i = 0; i < ARRAY_SIZE; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input salary: ";
        }
        int sale;
        std::cin >> sale;
        if (sale < 0) {
            std::cerr << "Error 1: Sales cannot be negative!" << std::endl;
            return;
        }
        const int salary = static_cast<int>(200 + sale * 0.09);
        const int counter = salary / 100 - 2;
        if (counter > 8) {
            ++array[8];
            continue;
        }
        ++array[counter];
    }
}

void
printArray(int array[], const int ARRAY_SIZE) 
{
    int counter = 0;
    for (int i = 200; i < 1100; i += 100) {
        if (1000 == i) {
            std::cout << "$1000 and over: " << array[ARRAY_SIZE - 1] << std::endl;
            break;
        }
        std::cout << i << " - " << i + 99 << ": " << array[counter] << std::endl;
        ++counter;
    }
}

