#include <iostream>
#include <unistd.h>
#include <string>

void reverseString(std::string array, const int firstNumber, const int lastNumber);

int
main()
{
    std::string array;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input text: ";
    }

    std::getline(std::cin, array);
    const int firstNumber = 0;
    const int lastNumber = array.size();

    reverseString(array, firstNumber, lastNumber);
    std::cout << std::endl;
    return 0;
}

void
reverseString(std::string array, const int firstNumber, const int lastNumber)
{
    if (firstNumber == lastNumber) {
        return;
    }
    std::cout << array[lastNumber - 1];
    reverseString(array,firstNumber, lastNumber - 1);
}
