#include <cmath>
#include <iostream>

void printArray(int checkArray[], const int rollTimes);

int
main()
{
    const int ARRAY_SIZE = 11;
    int checkingArray[ARRAY_SIZE] = {0};
    const int rollTimes = 36000;
    for (int i = 0; i < rollTimes; ++i) {
        const int dice1 = 1 + ::rand() % 6;
        const int dice2 = 1 + ::rand() % 6;
        const int sum = dice1 + dice2;
        ++checkingArray[sum - 2];

    }
    printArray(checkingArray, rollTimes);
    return 0;
}

void
printArray(int checkingArray[], const int rollTimes)
{
    for (int i = 2; i < 13; ++i) {
        std::cout << i << "\t" << checkingArray[i - 2] << "  " << static_cast<double>(checkingArray[i - 2] * 100) / rollTimes << '%' << std::endl;
    }
}
