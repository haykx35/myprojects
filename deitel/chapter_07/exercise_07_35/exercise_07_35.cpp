#include <iostream>
#include <unistd.h>

void printArray(const int array[], const int start, const int end);
void getArrayElements(int array[], const int size);

int
main()
{
    const int SIZE = 20;
    int array[SIZE] = {};
    getArrayElements(array, SIZE);
    printArray(array, 0, SIZE);
    std::cout << std::endl;
    return 0;
}

inline void
getArrayElements(int array[], const int size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << size << " integers: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

inline void
printArray(const int array[], const int start, const int end)
{
    if (end == start) {
        return;
    }
    std::cout << array[start] << " ";
    printArray(array, start + 1, end);
}


