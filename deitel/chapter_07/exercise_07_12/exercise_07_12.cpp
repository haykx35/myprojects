#include <unistd.h>
#include <iostream>

void bubbleSort(int array[], const int size);
void printSortedArray(int array[], const int size);
void inputArray(int array[], const int size);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE] = {};
    
    inputArray(array, ARRAY_SIZE);
    bubbleSort(array, ARRAY_SIZE);
    printSortedArray(array, ARRAY_SIZE);
    return 0;
}

void
inputArray(int array[], const int size)
{
    for (int i = 0; i < size; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter number: ";
        }
        std::cin >> array[i];
    }
}
void
bubbleSort(int array[], const int size)
{
    for (int i = 0; i < size; ++i) {
        int counter = 0;
        for (int j = 0; j < size - 1 - i; ++j) {
            if (array[(j + 1)] < array[j]) {
                int number1 = array[j + 1];
                array[j + 1] = array[j];
                array[j] = number1;
                ++counter;
            }
        }
        if (0 == counter) {
            return;
        }
    }
}

void
printSortedArray(int array[], const int size)
{
    for (int i = 0; i < size; ++ i) {
        std::cout << array[i] << std::endl;
    }
}
