#include "IntegerSet.hpp"
#include <cassert>
#include <iostream>

bool
IntegerSet::isEqualTo(const IntegerSet& lhv, const IntegerSet& rhv)
{
    for (int i = 0; i < SIZE; ++i) {
        if (lhv.arraySet_[i] != rhv.arraySet_[i]) {
            return false;
        }
    }
    return true;
}

IntegerSet::IntegerSet()
{
    emptySet();
}

IntegerSet::IntegerSet(const IntegerSet& rhv)
{
    emptySet();
    for (int i = 0; i < IntegerSet::SIZE; ++i) {
        arraySet_[i] = rhv.arraySet_[i];
    }
}

IntegerSet::IntegerSet(const int number1)
{
    emptySet();
    insertElement(number1);
    
}

IntegerSet::IntegerSet(const int* array, const int size)
{
    emptySet();
    assert(size < SIZE);
    for (int i = 0;  i < size; ++i) {
        assert(array[i] < SIZE);
        arraySet_[array[i]] = true;
    }
}

IntegerSet
IntegerSet::unionOfSets(const IntegerSet& rhv)
{
    IntegerSet result;
    for (int i = 0; i < SIZE; ++i) {
        result.arraySet_[i] = (arraySet_[i] || rhv.arraySet_[i]);
    }
    return result;
}

IntegerSet
IntegerSet::intersectionOfSets(const IntegerSet& rhv)
{
    IntegerSet result;
    for (int i = 0; i < SIZE; ++i) {
        result.arraySet_[i] = (arraySet_[i] && rhv.arraySet_[i]);
    }
    return result;
}

void
IntegerSet::insertElement(const int number1)
{
    assert(number1 < SIZE);
    arraySet_[number1] = true;
}


void
IntegerSet::deleteElement(const int number1)
{
    assert(number1 < SIZE);
    arraySet_[number1] = false;
}

void
IntegerSet::emptySet()
{
    for (int i = 0; i < IntegerSet::SIZE; ++i) {
        arraySet_[i] = false;
    }
}

void
IntegerSet::printSet() const
{
    for (int i = 0; i < SIZE; ++i) {
        if (arraySet_[i]) { 
            std::cout << '1' << " ";
        } else {
            std::cout << "_" << " ";
        }
        if (0 == (i + 1) % 20) {
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
}


