#include "IntegerSet.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    IntegerSet integerSet1;
    std::cout << "Set1 after default initialization" << std::endl;
    integerSet1.printSet();
    for (int i = 0; i < IntegerSet::SIZE; ++i) {
        integerSet1.insertElement(i);
    }
    std::cout << "Set1 after setting all elements" << std::endl;
    integerSet1.printSet();
    for (int i = 0; i < IntegerSet::SIZE; i += 2) {
        integerSet1.deleteElement(i);
    }
    std::cout << "Set1 after deleting each even element" << std::endl;
    integerSet1.printSet();
    IntegerSet integerSet2(50);
    std::cout << "Set2" << std::endl;
    integerSet2.printSet();
    IntegerSet integerSet3 = integerSet1.unionOfSets(integerSet2);
    std::cout << "Set3 after Set1 and Set2 union" << std::endl;
    integerSet3.printSet();
    IntegerSet integerSet4 = integerSet3.intersectionOfSets(integerSet2);
    std::cout << "Set4 after Set3 and Set2 intersection" << std::endl;
    integerSet4.printSet();
    IntegerSet integerSet5(integerSet2);
    std::cout << "Set5 after copy of Set2" << std::endl;
    integerSet5.printSet();
    IntegerSet integerSet6;
    std::cout << "Set6 after default initialization" << std::endl;
    integerSet6.printSet();
    std::cout << "Set5 is equal to Set6: " << std::boolalpha << IntegerSet::isEqualTo(integerSet6, integerSet5) << std::endl;
    integerSet6.insertElement(50);
    std::cout << "Set5" << std::endl;
    integerSet5.printSet();
    std::cout << "Set6" << std::endl;
    integerSet6.printSet();
    std::cout << "Set5 is equal to Set6: " << IntegerSet::isEqualTo(integerSet6, integerSet5) << std::endl;
    std::cout << "Set6" << std::endl;
    const int array[] = {1, 2, 3, 4, 5, 6};
    const int arraySize = sizeof(array) / sizeof(int);
    IntegerSet integerSet7(array, arraySize);
    integerSet7.printSet();

    return 0;
}
