#ifndef __INTEGERSET_HPP__
#define __INTEGERSET_HPP__

class IntegerSet
{
public:
    static const int SIZE = 100;
    static bool isEqualTo(const IntegerSet& lhv, const IntegerSet& rhv);
public:
    IntegerSet();
    IntegerSet(const IntegerSet& rhv);
    IntegerSet(const int number1);
    IntegerSet(const int* array, const int SIZE);
    IntegerSet unionOfSets(const IntegerSet& rhv);
    IntegerSet intersectionOfSets(const IntegerSet& rhv);
    void insertElement(const int number1);
    void deleteElement(const int number1);
    void printSet() const;
private:
    void emptySet();
private:
    bool arraySet_[SIZE];
};

#endif /// __INTEGERSET_HPP__
