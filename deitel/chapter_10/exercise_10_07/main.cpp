#include "Date.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input date: ";
    }
    std::string dateStr;
    std::getline(std::cin, dateStr);
    if (Date::ERROR == Date::getFormat(dateStr)) {
        std::cerr << "Error 1: Invalid date!" << std::endl;
        return 1;
    }

    Date date1 = dateStr.c_str();
    date1.printFormat1();
    date1.printFormat2();
    date1.printFormat3();
    return 0;
}

