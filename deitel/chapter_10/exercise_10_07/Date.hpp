#ifndef __DATE_HPP__
#define __DATE_HPP__
#include <string>
class Date
{
public:
    enum Format {FORMAT1, FORMAT2, FORMAT3, ERROR};
    static Format getFormat(const std::string& s);
    static bool isFormat1(const std::string& s);
    static bool isFormat2(const std::string& s);
    static bool isFormat3(const std::string& s);
public:
    Date(const char* str);
    Date(Format f, const char* str);
    void printFormat1() const;
    void printFormat2() const;
    void printFormat3() const;
private:
    void setFormat1(const std::string& s);
    void setFormat2(const std::string& s);
    void setFormat3(const std::string& s);
private:
    int month_;
    int day_;
    int year_;
};

#endif ///__DATE_HPP__

