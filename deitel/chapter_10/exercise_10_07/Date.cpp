#include "Date.hpp"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <cassert>

template <typename T>
T
strTo(const std::string& t)
{
    std::stringstream s(t);
    T result;
    s >> result;
    return result;
}

int
getNumber(const std::string& s, const int begin, const int end)
{
    int result = 0;
    for (int i = end - 1, radix = 1; i >= begin; --i, radix *= 10) {
        if (!::isdigit(s[i])) return -1;
        result += static_cast<int>(s[i] - '0') * radix;
        
    }
    return result;
}

const int MONTH_COUNT = 13;
const std::string MONTH_NAMES[MONTH_COUNT] = { " ", "Junuary", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
static const int daysPerMonth[13] = { 0, 31, 30, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

int
getMonthNumber(const std::string& s)
{
    std::stringstream month;
    month << s;
    std::string monthName;
    month >> monthName;
    for (int i = 1; i < MONTH_COUNT; ++i){
        if (monthName == MONTH_NAMES[i]) {
            return i;
        }
    }
    return 0;
}

bool
isLeapYear(const int yyyy)
{
    return ((yyyy % 400 == 0 || (yyyy % 4 == 0 && yyyy % 100 != 0)));
}

Date::Format
Date::getFormat(const std::string& s)
{
    /// Format 1: DDD YYYY
    /// Format 2: MM/DD/YY
    /// Format 3: June 14, 1992
    if (isFormat1(s)) {
        return FORMAT1;
    }
    if (isFormat2(s)) {
        return FORMAT2;
    }
    if (isFormat3(s)) {
        return FORMAT3;
    }
    return ERROR;
}

bool
Date::isFormat1(const std::string& s) 
{ /// DDD YYYY
    if (s.size() != 8) {
        return false;
    }

    if (s[3] != ' ') {
        return false;
    }

    const int yyyy = getNumber(s, 4, 8);
    if (yyyy < 0) {
        return false;
    }
 
    const int daysOfYear = isLeapYear(yyyy) ? 366 : 365;
    const int ddd = getNumber(s, 0, 3);
    if (ddd < 1 || ddd > daysOfYear) {
        return false;
    }
 
    return true;
}

bool
Date::isFormat2(const std::string& s)
{ /// MM/DD/YY
    if (s.size() != 8) {
        return false;
    }

    if (s[2] != '/' || s[5] != '/') {
        return false;
    }

    const int mm = getNumber(s, 0, 2);
    if (mm < 1 || mm > 12) {
        return false;
    }

    const int dd = getNumber(s, 3, 5);
    if (dd < 1 || dd > daysPerMonth[mm]) {
        return false;
    }

    const int yy = getNumber(s, 6, 8);
    if (yy < 0) {
        return false;
    }

    return true;
}

bool
Date::isFormat3(const std::string& s)
{ /// June 14, 1992
    const int mm = getMonthNumber(s);
    if (mm < 1 || mm > 12) {
        return false;
    }

    const int d = MONTH_NAMES[mm].length();
    if (s.size() != static_cast<size_t>(d + 9)) {
        return false;
    }

    if (s[d] != ' ' || s[d + 3] != ',' || s[d + 4] != ' ') {
        return false;
    }

    const int dd = getNumber(s, d + 1, d + 3);
    if (dd < 1 || dd > daysPerMonth[mm]) {
        return false;
    }

    const int yyyy = getNumber(s, d + 5, d + 9);
    if (yyyy < 0) {
        return false;
    }

    return true;
}

Date::Date(const char* str)
    : month_(1)
    , day_(1)
    , year_(2000)
{
    Format format =  getFormat(str);
    switch(format) {
    case FORMAT1: setFormat1(str); break;
    case FORMAT2: setFormat2(str); break;
    case FORMAT3: setFormat3(str); break;
    default: assert(0);    break;
    }
}

Date::Date(Format f, const char* str)
    : month_(1)
    , day_(1)
    , year_(2000)
{
    switch(f) {
    case FORMAT1: setFormat1(str); break;
    case FORMAT2: setFormat2(str); break;
    case FORMAT3: setFormat3(str); break;
    default: assert(0);    break;
    }
}

void
Date::setFormat1(const std::string& s)
{
    year_ = strTo<int>(s.substr(4, 4));
    day_ = strTo<int>(s.substr(0, 3));
    int i = 1;
    for ( ; day_ > daysPerMonth[i]; ++i) {
        day_ -= daysPerMonth[i];
    }
    month_ = i;
}

void
Date::setFormat2(const std::string& s)
{
    day_ = strTo<int>(s.substr(3, 2));
    month_ = strTo<int>(s.substr(0, 2));
    year_ = strTo<int>(s.substr(6, 2)) + 2000;

}

void
Date::setFormat3(const std::string& s)
{
    month_ = getMonthNumber(s);
    const int d = MONTH_NAMES[month_].length();
    day_ = strTo<int>(s.substr(d + 1, d + 3));
    year_ = strTo<int>(s.substr(d + 5, d + 9));
}

void
Date::printFormat1() const 
{
    int daysFormat1 = 0;
    for (int i = 1; i < month_; ++i) {
        daysFormat1 += daysPerMonth[i];
    }
    daysFormat1 += day_;
    std::cout << std::setfill('0') << std::setw(3) << daysFormat1 << ' ' << std::setw(4) << year_ << std::setfill(' ') << std::endl;

}

void
Date::printFormat2() const
{
    std::cout << std::setfill('0') << std::setw(2) << month_ << '/' << std::setw(2) << day_ << '/' << std::setw(2) << year_ << std::setfill(' ') << std::endl;
}

void
Date::printFormat3() const
{
    std::cout <<  MONTH_NAMES[month_] << ' '  << std::setfill('0') << std::setw(2) << day_ << ", " << std::setw(4) << year_ << std::setfill(' ') << std::endl;
}

