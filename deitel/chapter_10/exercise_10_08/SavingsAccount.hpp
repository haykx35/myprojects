#ifndef __SAVINGSACCOUNT_HPP__
#define __SAVINGSACCOUNT_HPP__ 
#include <string>

class SavingsAccount
{
private:
    static int annualInterestRate_;
public:
    static void modifyInterestRate(const int rate);
public:
    SavingsAccount(const std::string& name = "", const int savingsBalance = 0);
    void printBalance() const;
    void setSavingsBalance(const int savingsBalance);
    void calculateMonthlyInterest();
    int getSavingsAccount() const;
private:
    std::string name_;
    int savingsBalance_;
};

#endif /// __SAVINGSACCOUNT_HPP__ 
