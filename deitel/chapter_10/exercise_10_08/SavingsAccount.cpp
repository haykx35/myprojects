#include "SavingsAccount.hpp"
#include <cassert>
#include <iostream>
#include <iomanip>

int SavingsAccount::annualInterestRate_ = 0; 

void
SavingsAccount::modifyInterestRate(const int rate)
{
    assert (rate >= 0);
    annualInterestRate_ = rate;
}


SavingsAccount::SavingsAccount(const std::string& name, const int savingsBalance)
    : name_(name)
    , savingsBalance_(savingsBalance)
{
    setSavingsBalance(savingsBalance);
}

void
SavingsAccount::setSavingsBalance(const int savingsBalance)
{
    assert(savingsBalance >= 0);
    savingsBalance_ = savingsBalance;
}

int
SavingsAccount::getSavingsAccount() const
{
    return savingsBalance_;
}

void 
SavingsAccount::calculateMonthlyInterest()
{
    const int newBalance = savingsBalance_ + savingsBalance_ * annualInterestRate_ / 12 / 100;
    setSavingsBalance(newBalance);
}

void
SavingsAccount::printBalance() const
{
    std::cout << "The current balance of " << name_ << " is: " << std::fixed << std::setprecision(2) << getSavingsAccount() << "$" << std::endl;
}


