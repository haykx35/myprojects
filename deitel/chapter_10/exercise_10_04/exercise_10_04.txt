A friend function of a class is defined outside that class's scope, yet has the right to access the non- public (and public ) members of the class. Standalone functions or
entire classes may be declared to be friends of another class.
Using friend functions can enhance performance. 
Even though the prototypes for friend functions appear in the class definition, friends are not member functions.
Friendship is granted, not taken, for class B to be a friend of class A, class A must explicitly declare that class B is its friend.
Some people in the OOP community feel that "friendship" corrupts information hiding and weakens the value of the object-oriented
design approach. 
