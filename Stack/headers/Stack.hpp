#ifndef __STACK_HPP__
#define __STACK_HPP__
#include <vector>
#include <iostream>

namespace cd03 {
template <typename T>
class Stack
{
    template <typename TT>
    friend bool operator==(const Stack<TT>& lhv, const Stack<TT>& rhv);
    template <typename TT>
    friend bool operator<(const Stack<TT>& lhv, const Stack<TT>& rhv);
public:
    typedef T value_type;
    typedef std::vector<value_type> container_type;
    typedef size_t size_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;

    Stack();
    Stack(const Stack& rhv);
    const Stack& operator=(const Stack& rhv);
    bool empty() const;
    size_type size() const;
    reference top();
    const_reference top() const;
    void push(const_reference value);
    void pop();
private:
    container_type buffer_;
};
} // namespace cd03
#include "sources/Stack.cpp"

#endif ///__STACK_HPP__
