#include "headers/Stack.hpp"

#include <gtest/gtest.h>

TEST(Vector, Push_Pop_Top)
{
    cd03::Stack<int> s2;
    cd03::Stack<int> s3;
    EXPECT_EQ(s2.empty(), true);

    cd03::Stack<int> s1;
    s1.push(1);
    s1.push(2);
    s1.push(3);
    s1.push(4);
    s1.push(5);
    EXPECT_EQ(s1.size(), 5);
    EXPECT_EQ(s1.top(), 5);
    s1.pop();
    EXPECT_EQ(s1.top(), 4);
    EXPECT_EQ(s1.size(), 4);
    s1.pop();
    s1.pop();
    EXPECT_EQ(s1.size(), 2);
    EXPECT_EQ(s1.empty(), false);
    EXPECT_EQ(s1.top(), 2);
    s1 = s3;
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

