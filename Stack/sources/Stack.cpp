#ifndef __STACK_CPP__
#define __STACK_CPP__

#include "headers/Stack.hpp"
#include <iostream>
#include <cassert>

namespace cd03 {

template <typename T>
bool
operator==(const Stack<T>& lhv, const Stack<T>& rhv)
{
    return lhv.buffer_ == rhv.buffer_;
}

template <typename T>
bool
operator<(const Stack<T>& lhv, const Stack<T>& rhv)
{
    return lhv.buffer_ < rhv.buffer_;
}

template <typename T>
Stack<T>::Stack()
    : buffer_()
{
}

template <typename T>
Stack<T>::Stack(const Stack& rhv)
    : buffer_(rhv.buffer_)
{
}

template <typename T>
const Stack<T>&
Stack<T>::operator=(const Stack& rhv)
{
    if (&*this == &rhv) {
        return *this;
    }
    buffer_ = rhv.buffer_;
    return *this;
}

template <typename T>
bool
Stack<T>::empty() const
{
    return buffer_.empty();
}

template <typename T>
typename Stack<T>::size_type
Stack<T>::size() const
{
    return buffer_.size();
}

template <typename T>
typename Stack<T>::reference
Stack<T>::top()
{
    assert(!empty());
    return buffer_.back();
}

template <typename T>
typename Stack<T>::const_reference
Stack<T>::top() const
{
    assert(!empty());
    return buffer_.back();
}

template <typename T>
void
Stack<T>::push(const_reference value)
{
    buffer_.push_back(value);    
}

template <typename T>
void
Stack<T>::pop()
{
    buffer_.pop_back();
}
} // namespace cd03
#endif ///__STACK_CPP__
