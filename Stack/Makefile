progname=stack
lib=stack.a
utest=utest_$(progname)
CXX=g++
SYSTEM_LIB_PATH=/usr/local/bin
CXXFLAGS=-Wall -Wextra -Werror -std=c++03 -I. -L$(SYSTEM_LIB_PATH)

HEADER_DIR=headers
SOURCE_DIR=sources
TEST_DIR=tests

debug:   CXXFLAGS+=-g3
release: CXXFLAGS+=-g0 -DNDEBUG

BUILDS=builds
BUILD_DIR=$(BUILDS)
#debug:   BUILD_DIR+=$(BUILDS)/debug
#release: BUILD_DIR+=$(BUILDS)/release

SOURCES:=$(wildcard $(SOURCE_DIR)/*.cpp)
DEPENDS:=$(patsubst %.cpp,$(BUILD_DIR)/%.d,$(SOURCES))
PREPROCS:=$(patsubst %.cpp,$(BUILD_DIR)/%.ii,$(SOURCES))
ASSEMBLES:=$(patsubst %.cpp,$(BUILD_DIR)/%.s,$(SOURCES))
OBJS:=$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(SOURCES))

UTEST_SOURCES:=main_utest.cpp $(wildcard $(SOURCE_DIR)/*.cpp)
UTEST_DEPENDS:=$(patsubst %.cpp,$(BUILD_DIR)/%.d,$(UTEST_SOURCES))
UTEST_PREPROCS:=$(patsubst %.cpp,$(BUILD_DIR)/%.ii,$(UTEST_SOURCES))
UTEST_ASSEMBLES:=$(patsubst %.cpp,$(BUILD_DIR)/%.s,$(UTEST_SOURCES))
UTEST_OBJS:=$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(UTEST_SOURCES))

TEST_INPUTS:=$(wildcard $(TEST_DIR)/test*.input)
TESTS:=$(patsubst $(TEST_DIR)/%.input,%,$(TEST_INPUTS))

debug: $(BUILD_DIR) utest
release: $(BUILD_DIR) $(BUILD_DIR)/$(lib)

utest: $(BUILD_DIR)/$(utest)
	./$<

qa: $(TESTS)

test%: $(BUILD_DIR)/$(progname)
	./$< < $(TEST_DIR)/$@.input > $(BUILD_DIR)/$(TEST_DIR)/$@.output || echo "Negative Test..."
	diff $(BUILD_DIR)/$(TEST_DIR)/$@.output $(TEST_DIR)/$@.expected && echo PASSED || echo FAILED

#$(CXX)    $(CXXFLAGS) $^ -o $@
$(BUILD_DIR)/$(lib): $(OBJS) | .gitignore
	ar -crv $@ $^

$(BUILD_DIR)/$(utest): $(UTEST_OBJS) | .gitignore
	$(CXX)    $(CXXFLAGS) $^ -L/usr/local/lib -lgtest -lpthread -o $@

$(BUILD_DIR)/%.ii: %.cpp
	$(CXX) -E $(CXXFLAGS) $< -o $@
	$(CXX) $(CXXFLAGS) -MT $@ -MM $< > $(patsubst %.ii,%.d,$@)

%.s: %.ii
	$(CXX) -S $(CXXFLAGS) $< -o $@

%.o: %.s
	$(CXX) -c $(CXXFLAGS) $< -o $@

.gitignore:
	echo $(progname) > .gitignore
	echo $(utest) >> .gitignore

clean:
	rm -rf $(BUILD_DIR) .gitignore

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)/$(SOURCE_DIR) $(BUILD_DIR)/$(TEST_DIR)

.PRECIOUS: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)

sinclude $(DEPENDS) $(UTEST_DEPENDS)

