#ifndef __T__SLIST__HPP_
#define __T__SLIST__HPP_

#include <iostream>

namespace cd03 {
template <typename T>
class Slist
{
public:
    /// Container
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;
private:
    struct Node {
    public:
        Node();
        Node(const_reference data);
        Node(const_reference data, Node* ptr);
    public:
        Node* next_;
        T data_;
    };
public:
    class const_iterator {
        friend Slist;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const_iterator& operator=(const const_iterator& rhv);
        bool operator!=(const const_iterator& rhv) const;
        bool operator==(const const_iterator& rhv) const;
        const_iterator& operator++();
        const_iterator operator++(int);
        const_reference operator*() const;
        const_reference operator->() const;
    protected:
        void setPtr(Slist::Node* ptr);
        Slist::Node* getPtr() const;
        explicit const_iterator(Slist::Node* ptr);
    private:
        Slist::Node* ptr_;
        };
    class iterator : public const_iterator {
        friend Slist;
    public:
        iterator();
        iterator(const iterator& rhv);
        iterator& operator=(const iterator& rhv);
        reference operator*();
        iterator& operator++();
        iterator operator++(int);
        reference operator->();
    private:
        explicit iterator(Slist::Node* ptr);
    };
public:
    Slist();
    Slist(const size_type s);
    Slist(const size_type s, const_reference data);
    Slist(const Slist& rhv);	
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    template <class InputIterator>
    Slist(InputIterator f, InputIterator l);
    ~Slist();
    Slist& operator=(const_reference rhv);
    reference front();
    reference back();
    const_reference front() const;
    const_reference back() const;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const_reference data);
    void insert_after(iterator pos, size_type n, const value_type& x);
    template<class InputIterator>
    void insert_after(iterator pos, InputIterator f, InputIterator l);
    iterator erase_after(iterator pos);
    iterator erase_after(iterator before_first, iterator last);
    void push_front(const_reference);
    void push_back(const_reference);
    void pop_front();
    void pop_back();
    void swap(Slist&);
    void resize(const size_type s, const_reference data);
    void resize(const size_type s);
    void clear();
protected:
    void advance(Node*& node, size_t s);
private:
    Node* begin_;
    Node* end_;

};
} /// namespace cd03
#include "sources/Slist.cpp"
#endif ///__T__SLIST__HPP_
