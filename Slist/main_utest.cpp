#include "headers/Slist.hpp"

#include <gtest/gtest.h>

TEST(Slist, DefaultConstructor)
{
    cd03::Slist<int> l;
    EXPECT_EQ(l.size(), 0);
}

TEST(List, SizeConstructor) 
{
    cd03::Slist<int> l(5);
    EXPECT_EQ(l.size(),5);
}

TEST(Slist, initialValueConstructor)
{
    cd03::Slist<int> l(5u,6);
    EXPECT_EQ(l.size(),5);
}

TEST(Slist, Front)
{
    cd03::Slist<int> l(5u,6);
    EXPECT_EQ(l.front(),6);
}

TEST(Slist, Back)
{
    cd03::Slist<int> l(5u,6);
    EXPECT_EQ(l.back(),6);
}

TEST(Slist, InputIteratorConstructor)
{
    typedef cd03::Slist<int> L;
    typedef L::iterator LI;
    L l1(5u,6);
    LI f = ++l1.begin();
    LI l = l1.end();
    L l2(f,l);
    EXPECT_EQ(l2.size(), 4);
    for (LI i = l2.begin(); i != l2.end(); ++i) {
        EXPECT_EQ(*i, 6);
    }
}
TEST(Slist, push_back)
{
    cd03::Slist<int> l(5u,6);
    l.push_back(7);
    EXPECT_EQ(l.size(),6);
}

TEST(Slist, push_front)
{
    cd03::Slist<int> l(5u,6);
    l.push_front(7);
    EXPECT_EQ(l.size(),6);
}

TEST(Slist, pop_front)
{
    cd03::Slist<int> l(5u,6);
    l.pop_front();
    EXPECT_EQ(l.size(),4);
}

TEST(Slist, pop_back)
{
    cd03::Slist<int> l(5u,6);
    l.pop_back();
    EXPECT_EQ(l.size(),4);
}

TEST(Slist, Resize)
{
    cd03::Slist<int> l(5u,6);
    l.resize(10,10);
    EXPECT_EQ(l.size(),10);
    l.resize(4);
    EXPECT_EQ(l.size(),5);
}

TEST(Slist, Iterator)
{
    typedef cd03::Slist<int> V;
    typedef V::iterator It;
    V v(4);
    V v1(4u, 4);
    int value = 1;
    for (It it = v.begin(); it != v.end(); ++it, ++value) {
        *it = value;
    }
    for (It it = v1.begin(); it != v1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = v.begin();
    EXPECT_EQ(*it, 1);
    EXPECT_EQ(*++it, 2);
    EXPECT_EQ(*it, 2);
    EXPECT_EQ(*it++, 2);
    EXPECT_EQ(*it, 3);
}

TEST(Slist, InsertInputIterator)
{
    typedef cd03::Slist<int> L; 
    typedef L::iterator IT;
    L l1(5u,5);
    IT f = ++l1.begin();
    IT l = l1.end();
    L l2(5u,6);
    IT it = l2.begin();
    l2.insert_after(it,f,l);
    EXPECT_EQ(l2.size(),9);
    EXPECT_EQ(*(l2.begin()),6);
    EXPECT_EQ(*(++l2.begin()),5);
}

TEST(Slist, ConstIterator)
{
    typedef cd03::Slist<int> V;
    typedef V::const_iterator It;

    V v;
    V v1(4u, 4);
    v.resize(1, 1);
    v.resize(2, 2);
    v.resize(3, 3);
    v.resize(4, 4);
    for (It it = v1.begin(); it != v1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = v.begin();
    EXPECT_EQ(*it, 1);
    ++it;
    EXPECT_EQ(*it, 2);
    EXPECT_EQ(*it, 2);
    EXPECT_EQ(*it++, 2);
    EXPECT_EQ(*it, 3);
}


TEST(Slist, Insert)
{
    typedef cd03::Slist<int> V;
    typedef V::iterator VI;
    V v1(5u,5);
    VI it = v1.begin();
    EXPECT_EQ(*it, 5);
    it = v1.begin();
    it = v1.insert_after(it, 6);
    EXPECT_EQ(*it, 6);
}

TEST(Slist, erase)
{
    typedef cd03::Slist<int> V;
    typedef V::iterator VI;
    int value = 1;
    V v(7);
    for (VI it = v.begin(); it != v.end(); ++it, ++value) {
        *it = value;
    }
    VI it2 = v.begin();
    EXPECT_EQ(v.empty(), false);
    EXPECT_EQ(v.size(), 7);
    EXPECT_EQ(*it2, 1);
    EXPECT_EQ(*++it2, 2);
    EXPECT_EQ(*it2, 2);
    VI it3 = v.begin();
    EXPECT_EQ(*(v.erase_after(it3)), 1);
    EXPECT_EQ(*++it3, 3);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

