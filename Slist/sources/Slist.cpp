#ifndef __T__SLIST_CPP__
#define __T__SLIST_CPP__

#include <iostream>
#include <cassert>
#include "headers/Slist.hpp"

namespace cd03 {

template <typename T>
Slist<T>::Slist()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
Slist<T>::Slist(const size_type s, const_reference data)
    : begin_(NULL)
    , end_(NULL)
{
    resize(s, data);
}

template <typename T>
Slist<T>::Slist(const size_type s)
    : begin_(NULL)
    , end_(NULL)
{
    resize(s);
}

template <typename T>
template <typename InputIterator>
Slist<T>::Slist(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
{
    for (InputIterator i = f; i != l; ++i) {
        push_back(*i);
    }
}

template <typename T>
Slist<T>::Node::Node()
    : next_(NULL)
    , data_()
{

}

template <typename T>
Slist<T>::Node::Node(const_reference data)
    : next_(NULL)
   , data_(data)
{

}

template <typename T>
Slist<T>::Node::Node(const_reference data, Slist<T>::Node* ptr)
    : next_(ptr)
    , data_(data)
{

}

template <typename T>
Slist<T>::~Slist()
{
    clear();
}

template <typename T>
void
Slist<T>::resize(const size_type s)
{
    if (0 == s)
        clear();
    Node* temp = begin_;
    size_type lSize = size();
    for (size_type i = lSize; i < s; ++i) {
        push_back(T());
    }
    if (lSize > s) {
        advance(temp,s);
        end_ = temp;
        temp = temp->next_;
        end_ -> next_ = NULL;
        while (temp != NULL) {
            Node* temp2 = temp;
            temp = temp->next_;
            delete temp2;
        }
    }
}

template <typename T>
void
Slist<T>::resize(const size_type s, const_reference data)
{
    if (0 == s) 
        clear();
    Node* temp = begin_;
    size_type lSize = size();
    for (size_type i = lSize; i < s; ++i) {
        push_back(data);
    }
    if (lSize > s) {
        advance(temp,s);
        end_ = temp;
        temp = temp->next_;
        end_ -> next_ = NULL;
        while (temp != NULL) {
            Node* temp2 = temp;
            temp = temp->next_;
            delete temp2;
        }
    }
}

template<typename T>
size_t
Slist<T>::size() const
{
    size_t s = 0;
    for( Node* i = begin_; i != NULL; i = i->next_) {
        ++s;
    }
    return s;
}

template <typename T>
bool
Slist<T>::empty() const
{
    return (begin_ == NULL && end_ == NULL);
}

template <typename T>
void
Slist<T>::clear()
{
    while(begin_ != NULL) { 
        pop_front();
    }
}

template <typename T>
typename Slist<T>::reference
Slist<T>::front()
{
    assert(NULL != begin_);
    return begin_ -> data_;
}

template <typename T>
typename Slist<T>::const_reference
Slist<T>::front() const
{
    assert(NULL != begin_);
    return begin_ -> data_;
}

template <typename T>
typename Slist<T>::reference
Slist<T>::back()
{
    assert(NULL != end_);
    return end_ -> data_;
}

template <typename T>
typename Slist<T>::const_reference
Slist<T>::back() const
{
    assert(NULL != end_);
    return end_ -> data_;
}

template <typename T>
void
Slist<T>::push_back(const_reference data)
{
    Node* temp = new Node(data);
    if (NULL != begin_) { 
        end_-> next_ = temp;
        end_ = temp;
    }
    if (NULL == begin_) {
        begin_ = temp;
        end_ = temp;
    }
}

template<typename T>
void
Slist<T>::push_front(const_reference data)
{
    Node* temp = new Node(data);
   if (NULL != begin_) { 
        temp->next_ = begin_;
        begin_ = temp;
    }
    if (NULL == begin_) {
        begin_ = temp;
        end_ = temp;
    }
}

template <typename T>
void
Slist<T>::pop_front()
{
    if (NULL == begin_) {
        return;
    }
    Node* temp = begin_;
    begin_ = begin_->next_;
    delete temp;
}

template <typename T>
void
Slist<T>::pop_back()
{
    if (NULL == end_) {
        return;
    }
    Node* temp = begin_;
    while (temp -> next_ !=end_){
        temp = temp->next_;
    }
    end_ = temp;
    temp = temp->next_;
    assert(temp != end_);
    delete temp;
    end_ -> next_ = NULL;
}

template <typename T>
typename Slist<T>::const_iterator
Slist<T>::begin() const 
{
    return const_iterator(begin_);
}

template <typename T>
typename Slist<T>::const_iterator
Slist<T>::end() const
{
    return const_iterator(end_-> next_);
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::end()
{
    return iterator(end_ -> next_); 
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::insert_after(iterator pos)
{
   return insert_after(pos, T());
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::insert_after(iterator pos, const_reference data)
{
    if(pos.ptr_ == end_) {
        push_back(data);
        return iterator(end_);
    }
    Node* temp = new Node(data);
    temp -> next_ = pos.ptr_ -> next_;
    pos.ptr_ -> next_ = temp; 
    return iterator(temp);
}

template <typename T>
template <typename InputIterator>
void
Slist<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    while (f!=l) {
        pos = insert_after(pos,*f);
        ++f;
    }
}

template <typename T>
void
Slist<T>::advance(Node*& node, size_t size)
{
    for (size_type i = 0; i < size; ++i) {
            node = node->next_;
    }
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::erase_after(iterator pos)
{
    Node* temp = pos.getPtr()->next_;
    pos.getPtr()->next_ = temp->next_;
    delete temp;
    return pos;
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::erase_after(iterator before_first, iterator last)
{
    for (iterator it = before_first; it != last; ++it) {
         erase_after(it);
    }
}

//ITERATOR//
template <typename T>
Slist<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
Slist<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
Slist<T>::iterator::iterator(Slist::Node* ptr)
    : const_iterator(ptr)
{
    
}

template <typename T>
typename Slist<T>::iterator&
Slist<T>::iterator::operator=(const iterator& rhv)
{
    const_iterator::setPtr(rhv.ptr_);
    return *this;
}

template <typename T>
typename Slist<T>::reference
Slist<T>::iterator::operator*()
{
    return const_iterator::getPtr() -> data_;
}

template <typename T>
typename Slist<T>::iterator&
Slist<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() -> next_);
    return *this;
}

template <typename T>
typename Slist<T>::iterator
Slist<T>::iterator::operator++(int)
{
    Node* temp = const_iterator::getPtr();
    iterator it(temp);
    const_iterator::setPtr(const_iterator::getPtr() -> next_);
    return it;
}

template <typename T>
typename Slist<T>::reference
Slist<T>::iterator::operator->()
{
    return const_iterator::getPtr() -> data_;
}

//ITERATOR//
//CONST_ITERATOR//
template <typename T>
Slist<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
Slist<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
Slist<T>::const_iterator::const_iterator(Slist::Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
typename Slist<T>::const_iterator&
Slist<T>::const_iterator::operator=(const const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
bool
Slist<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return (ptr_ == rhv.ptr_);
}

template <typename T>
bool
Slist<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return (ptr_ != rhv.ptr_);
}

template <typename T>
typename Slist<T>::const_iterator&
Slist<T>::const_iterator::operator++()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
typename Slist<T>::const_iterator
Slist<T>::const_iterator::operator++(int)
{
    Node* temp = ptr_;
    const_iterator it(temp);
    ptr_ = ptr_->next_;
    return it;
}

template <typename T>
typename Slist<T>::const_reference
Slist<T>::const_iterator::operator*() const
{
  return ptr_->data_; 
}

template <typename T>
typename Slist<T>::const_reference
Slist<T>::const_iterator::operator->() const
{
    return ptr_->data_;
}
template <typename T>
void
Slist<T>::const_iterator::setPtr(Slist::Node* ptr)
{
    ptr_ = ptr;
}
template <typename T>
typename Slist<T>::Node* 
Slist<T>::const_iterator::getPtr() const
{
    return ptr_;
}
//CONST_ITERATOR//
}/// namespace cd03
#endif /// __T__SLIST_CPP__
