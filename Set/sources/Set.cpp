#ifndef __SET_CPP__
#define __SET_CPP__

#include "headers/Set.hpp"
#include <iostream>
#include <cassert>

namespace cd03 {

template <typename Data>
Set<Data>::Set()
    :root_(NULL)
{
}

template <typename Data>
template <typename InputIterator>
Set<Data>::Set(InputIterator f, InputIterator l)
    : root_(NULL)
{
    while (f != l) {
        insert(*f);
        ++f;
    }
}

template <typename Data>
Set<Data>::~Set()
{
    clear();
}

template <typename Data>
Set<Data>::Node::Node()
    : data_()
    , parent_(NULL)
    , left_(NULL)
    , right_(NULL)
{
}

template <typename Data>
void
Set<Data>::clear()
{
    postOrderDelete(root_);
}

template <typename Data>
bool
Set<Data>::empty() const
{
    return root_ == NULL;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::getRoot() const
{
    return root_;
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::Root()
{
    return iterator(root_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::Root() const
{
    return const_iterator(root_);
}

template <typename Data>
typename Set<Data>::size_type
Set<Data>::size() const
{
   size_t s = 0;
   inOrder(root_, s);
   return s;
}

template <typename Data>
void
Set<Data>::postOrderDelete(Node*& n)
{
    if (n != NULL) {
        postOrderDelete(n->left_);
        postOrderDelete(n->right_);
        delete n;
    }
}

template <typename Data>
void
Set<Data>::preOrder(Node* n, size_t s)
{
    if (n != NULL) {
        visit(n);
        preOrder(n->left_, ++s);
        preOrder(n->right_, ++s);
    }
}

template <typename Data>
void
Set<Data>::preOrder(Node* n)
{
    if (n != NULL) {
        preOrder(n->left_);
        visit(n);
        preOrder(n->right_);
    }
}

template <typename Data>
void
Set<Data>::printSet(iterator it, const size_t size) const
{
    if (!it) {
        return;
    }
    printSet(it.right(), size + 1);
    for (size_t i = 0; i < size; ++i) {
        std::cout << "    ";
    }
    std::cout << it.getData() << '(';
    if (it.parent()) {std::cout << it.parent().ptr_ -> data_; } 
    std::cout << ")" << std::endl;
    printSet(it.left(), size + 1);
}

template <typename Data>
void
Set<Data>::inOrder(Node* n, size_t& s) const
{
    if (n != NULL) {
        inOrder(n->left_, s);
        ++s;
        inOrder(n->right_, s);
    }
}

template <typename Data>
void
Set<Data>::inOrder(Node* n)
{
    if (n != NULL) {
        inOrder(n->left_);
        visit(n);
        inOrder(n->right_);
    }
}

template <typename Data>
void
Set<Data>::visit(Node* n)
{
}

template <typename Data>
std::pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(const value_type& x) 
{
    Node* ptr = root_;
    iterator it(ptr);
    return insert(it, x);
}

template <typename Data>
std::pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(iterator& it, const value_type& x) 
{  
    std::pair<iterator, bool> pair1;
    if(!it) {
        it.ptr_ = new Node(x); 
        if (NULL == root_) {
        root_ = it.getPtr();
        }
        pair1 = std::make_pair(it,true);
        return pair1; 
    }
    goUp(it, x);
    const bool isAdded = goDown(it, x);
    balance(it);
    pair1 = std::make_pair(it, isAdded);
    return pair1; 
}

template <typename Data>
void
Set<Data>::erase(const key_type& x)
{
    iterator it = find(x);
    erase(it);
}

template <typename Data>
void
Set<Data>::erase(iterator it)
{
    iterator itP = it.parent();
    iterator itL = it.left();
    iterator itR = it.right();
    iterator itLRM(rightMost(itL.ptr_));
    iterator itRRM(rightMost(itR.ptr_));
    bool IsRightParent = it.isRightParent();
        itL.setParent(itP);
        itLRM.setRight(itR);
        if (IsRightParent) {
            itP.setLeft(itL);
        } else {
            itP.setRight(itL);
        }
        itR.setParent(itLRM);
        delete it.ptr_;
        balance(itRRM);
}

template <typename Data>
void 
Set<Data>::goUp(Set::iterator& it, const_reference x) 
{
    if (!it || !it.parent() || x == *it) {
        return;
    }
    const const_iterator temp =
        (x < *it) ? it.firstLeftParent()
                  : it.firstRightParent();
    if (temp.isRoot()) { return; }
    const bool isRightPlace =
        (x < *it) ? (*temp > x) : (*temp < x);
    if (isRightPlace) { return; }
    goUp(it = temp, x);
}

template <typename Data>
bool
Set<Data>::goDown(iterator& it, const_reference x) {
    if (x < *it) {
        if (!it.left()) { 
            it.createLeft(x); return true;
        }
        return goDown(it.goLeft(), x);
    }
    if (x > *it) {
        if (!it.right()) {
            it.createRight(x); return true;
        }
        return goDown(it.goRight(), x);
    }
    return false;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::leftMost(Node* n) const
{
    Node* temp = n;
    while(temp-> left_ != NULL) {
        temp = temp->left_;
    }
    return temp;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::rightMost(Node* n) const
{
    Node* temp = n;
    while(temp-> right_ != NULL) {
        temp = temp->right_;
    }
    return temp;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::firstLeftParent()
{
    const_iterator temp(ptr_);
    while(!temp.isLeftParent()) {temp.goParent();} 
    temp.goParent();
    return temp;

}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::firstRightParent()
{
    const_iterator temp(ptr_);
    while(!temp.isRightParent()) { temp.goParent();}
    temp.goParent();
    return temp;
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::find(const key_type& x) const
{
    Node* ptr = root_;
    while(x != ptr -> data_) {
        if (x > ptr -> data_) {
            ptr = ptr -> right_;
        }
        if (x < ptr -> data_) {
            ptr = ptr -> left_;
        }
    }
    return iterator(ptr);
}

template <typename Data>
void 
Set<Data>::balance(iterator& it) 
{
    if (!it) { return; }
    iterator tempLeft = it.left();
    iterator tempRight = it.right();
    const int factor = balance(it.ptr_); /// 2 - 0
    if (factor > 1) {
        if (balance(tempLeft.ptr_) < 0) { /// 0 - 1
            rotateLeft(tempLeft);
        }
        rotateRight(it);
    } else if (factor < -1) {
        if (balance(tempRight.ptr_) > 0) {
            rotateRight(tempRight);
        }
        rotateLeft(it);
        } 
     balance(it.goParent()); 
}

template <typename Data>
void 
Set<Data>::rotateLeft(iterator& it)
{
    iterator itParent = it.parent();
    iterator itRight = it.right();
    iterator itRightLeft = itRight.left();
    const bool isRightParent = it.isRightParent();
    it.setRight(itRightLeft);
    if (itRightLeft) { itRight.left().setParent(it); }
    itRight.setLeft(it);
    it.setParent(itRight);
    itRight.setParent(itParent);
    if (itParent) {
        isRightParent ? itParent.setLeft(itRight)
                      : itParent.setRight(itRight);
    } else { root_ = itRight.getPtr(); }
    it = itRight;
}

template <typename Data>
void 
Set<Data>::rotateRight(iterator& it) 
{
    iterator itParent = it.parent(), itLeft = it.left();
    iterator itLeftRight = itLeft.right();
    const bool isLeftParent = it.isLeftParent();
    it.setLeft(itLeftRight);
    if (itLeft.right()) { itLeft.right().setParent(it); }
    itLeft.setRight(it);
    it.setParent(itLeft);
    itLeft.setParent(itParent);
    if (itParent) {
        isLeftParent ? itParent.setRight(itLeft)
                     : itParent.setLeft(itLeft);
    } else { root_ = itLeft.getPtr(); }
    it = itLeft;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::begin() const
{
    return const_iterator(leftMost(root_));
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::end() const
{
    return const_iterator(NULL);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::begin()
{
    return iterator(leftMost(root_));
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::end()
{
    return iterator(NULL);
}

///CONST_ITERATOR///

template <typename Data>
Set<Data>::const_iterator::const_iterator(Set::Node* ptr)
    : ptr_(ptr)
{
}

template <typename Data>
Set<Data>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename Data>
Set<Data>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator=(const const_iterator& rhv)
{
    if(&*this == &rhv) {
        return *this;
    }
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename Set<T>::Node*
Set<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename Data>
void
Set<Data>::const_iterator::setPtr(Set::Node* ptr)
{
    ptr_ = ptr;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator*() const
{
    return ptr_ -> data_;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator++()
{
    ptr_ = nextInorder(ptr_);    
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator--()
{
    ptr_ = prevInorder(ptr_);   
    return *this;
}

template <typename Data>
bool
Set<Data>::const_iterator::isLeftParent() const
{  
    if (ptr_ -> parent_ == NULL) {
        return false;
    }
    if (ptr_ -> parent_ -> right_ == ptr_ ) {
        return true;
    }
    return false;
}

template <typename Data>
bool
Set<Data>::const_iterator::isRightParent() const
{
    if (ptr_ -> parent_ == NULL) {
        return false;
    }
    if (ptr_ -> parent_ -> left_ == ptr_) {
        return true;
    }
    return false;
}

template <typename Data>
bool
Set<Data>::const_iterator::isRoot() const
{
    return (ptr_ -> parent_ == NULL);
}

template <typename Data>
bool
Set<Data>::const_iterator::operator!() const
{
    return (ptr_ == NULL);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::parent()
{
    assert(ptr_ != NULL);
    return const_iterator(ptr_ -> parent_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::left()
{
    return const_iterator(ptr_ -> left_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::right()
{
    return const_iterator(ptr_ -> right_);
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::goLeft()
{
    ptr_ = ptr_ -> left_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::goRight()
{
    ptr_ = ptr_ -> right_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::goParent()
{
    ptr_ = ptr_ -> parent_;
    return *this;
}

template <typename Data>
int
Set<Data>::balance(Node* ptr)
{
   return height(ptr -> left_) - height(ptr -> right_); 
}

template <typename Data>
int
Set<Data>::height(Node* n) const
{
    if (NULL == n) { return -1;}
    return std::max(height(n->left_), height(n->right_)) + 1;
}

template <typename Data>
Set<Data>::const_iterator::operator bool() const
{
    return ptr_ != NULL;
}

template <typename Data>
void
Set<Data>::const_iterator::createLeft(Set::const_reference x)
{
    ptr_ -> left_ = new Node(x, ptr_, NULL, NULL);
    ptr_ = ptr_ -> left_;
}

template <typename Data>
void
Set<Data>::const_iterator::createRight(Set::const_reference x)
{
    ptr_ -> right_ = new Node(x, ptr_, NULL, NULL);
    ptr_ = ptr_ -> right_;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::const_iterator::prevInorder(Node* rhv) 
{    
    if (rhv->left_) {
        rhv = rhv->left_;
        while (rhv->rigth_ != NULL) {
            rhv = rhv->rigth_;
        }
        return rhv;
    }
    if (rhv->parent_ != NULL) {
        if(rhv->parent_->rigth_ == rhv) {
            rhv = rhv->parent_;
            return rhv;
        }
        while ((rhv->parent_) && rhv != rhv->parent_->rigth_) {
            rhv = rhv->parent_;
        }
        rhv = rhv->parent_;
        return rhv;
    }
    return rhv;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::const_iterator::nextInorder(Node* rhv) 
{
    if (rhv->right_) {
        rhv = rhv->right_;
        while (rhv->left_ != NULL) {
            rhv = rhv->left_;
        }
        return rhv;
    }
    if (rhv->parent_ != NULL) {
        if (rhv->parent_->left_ == rhv) {
            rhv = rhv->parent_;
            return rhv;
        }
        while ((rhv->parent_) && rhv != rhv->parent_->left_) {
            rhv = rhv->parent_;
        }
        rhv = rhv->parent_;
        return rhv;
    }
    return rhv;
}

///CONST_ITERATOR///
///ITERATOR///
template <typename Data>
Set<Data>::iterator::iterator()
    : const_iterator()
{
}

template <typename Data>
Set<Data>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv.ptr_)
{
}

template <typename Data>
Set<Data>::iterator::iterator(Set::Node* ptr)
    : const_iterator(ptr)
{
}

template <typename Data>
typename Set<Data>::reference
Set<Data>::iterator::operator*()
{
    return (const_iterator::getPtr() -> data_);
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::goRight()
{
    const_iterator::setPtr(const_iterator::getPtr() -> right_);
    return *this;
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::goLeft()
{
    const_iterator::setPtr(const_iterator::getPtr() -> left_);
    return *this;
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::goParent()
{
    const_iterator::setPtr(const_iterator::getPtr() -> parent_);
    return *this;
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::operator=(const const_iterator& rhv)
{
    if (&*this == &rhv) {
        return *this;
    }
    const_iterator::setPtr(rhv.ptr_);
    return *this;
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::operator=(const iterator& rhv)
{
    if (&*this == &rhv) {
        return *this;
    }
    const_iterator::setPtr(rhv.getPtr());
    return *this;
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::left()
{
    return iterator(const_iterator::getPtr() -> left_);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::right()
{
    return iterator(const_iterator::getPtr() -> right_);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::parent()
{
    return iterator(const_iterator::getPtr() -> parent_);
}

template <typename Data>
void
Set<Data>::const_iterator::setParent(const_iterator& it)
{
    ptr_ -> parent_ = it.ptr_;
}

template <typename Data>
void
Set<Data>::const_iterator::setLeft(const_iterator& it)
{
    ptr_ -> left_ = it.ptr_;
}

template <typename Data>
void
Set<Data>::const_iterator::setRight(const_iterator& it)
{
    ptr_ -> right_ = it.ptr_;
}

template <typename Data>
Data
Set<Data>::const_iterator::getData() const
{
    return ptr_ -> data_;
}

///ITERATOR///
} /// namespace cd03
#endif /// __SET_CPP__
