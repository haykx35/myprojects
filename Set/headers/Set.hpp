#ifndef __SET_HPP__
#define __SET_HPP__

#include <iostream>

namespace cd03 {
template <typename Data>
class Set {
public:
    /// Container
    typedef Data value_type;
    typedef Data key_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;
private:
    struct Node {
        Node();
        Node(const Data& data,
             Node* parent = NULL, 
             Node* left = NULL,
             Node* right = NULL)
            : data_(data)
            , parent_(parent)
            , left_(left)
            , right_(right)
        {}
        Data data_;
        Node* parent_;
        Node* left_;
        Node* right_;
    };
public:
    class const_iterator {
        friend Set;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const_iterator& operator=(const const_iterator& rhv);
        Set::Node* getPtr() const;
        Data getData() const;
        const_reference operator*() const;
        const_iterator& operator--(); ////have a problem
        const_iterator& operator++(); //// have a problem
        bool operator!() const;
        operator bool() const;
        bool isRightParent() const;
        bool isLeftParent() const;
        bool isRoot() const;
        const_iterator parent();
        const_iterator left();
        const_iterator right();
        const_iterator& goLeft();
        const_iterator& goRight();
        const_iterator& goParent();
        const_iterator firstLeftParent();
        const_iterator firstRightParent();
        void setPtr(Set::Node* ptr);
        void setParent(const_iterator& it);
        void setLeft(const_iterator& it);
        void setRight(const_iterator& it);
        void createLeft(Set::const_reference x);
        void createRight(Set::const_reference x);
    protected:
        explicit const_iterator(Set::Node* ptr);
        Set::Node* nextInorder(Node* node);
        Set::Node* prevInorder(Node* node);
    private:
        Node* ptr_;
    };
////////////////////////////////////////////////////////////////
public:
    class iterator : public const_iterator {
        friend Set;
    public:
        iterator();
        iterator(const iterator& rhv);
        reference operator*();
        iterator& operator=(const iterator& rhv);
        iterator& operator=(const const_iterator& rhv);
        iterator left();
        iterator right();
        iterator parent();
        iterator& goLeft();
        iterator& goRight();
        iterator& goParent();
    protected:
        explicit iterator(Set::Node* ptr);

    };
///////////////////////////////////////////////////////////////
public:
    Set();
    Set(const Set& rhv); /// Container
    template <typename InputIterator>
    Set(InputIterator f, InputIterator l); /// Seq
    ~Set();
    const Set& operator=(const Set&);
    void swap(Set&);
    size_type size() const; /// Container
    size_type max_size() const; /// Container
    bool empty() const; /// Container
    void clear();
    void visit(Node* n);
    void printSet(iterator it, const size_t size) const; 
    Node* leftMost(Node* n) const;
    Node* rightMost(Node* n) const;
    void goUp(iterator& it, const_reference x);
    bool goDown(iterator& it, const_reference x);
    iterator find(const key_type& x) const;
    std::pair<iterator, bool>
    insert(typename Set<Data>::iterator& it, const value_type& x);
    std::pair<iterator, bool> 
    insert(const value_type& x);
    void erase(const key_type& x);
    void erase(iterator it);
    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();
    int height(Node* ptr) const;
    Node* getRoot() const;
    const_iterator Root() const;
    iterator Root();
protected:
    void balance(iterator& it);
    void rotateLeft(iterator& it);
    void rotateRight(iterator& it);
    int balance(Node* ptr);
    void preOrder(Node* n, size_t s);
    void inOrder(Node* n, size_t& s) const;
    void preOrder(Node* n);
    void inOrder(Node* n);
    void postOrderDelete(Node*& n);
    void preOrderIterative(Node* n);
    void inOrderIterative(Node* n);
    void postOrderIterative(Node* n);
    void levelOrder(Node* n);
private:
    Node* root_;
};
} /// namespace cd03

#include "sources/Set.cpp"
#endif /// __SET_HPP__
