#include "headers/Set.hpp"
#include <vector>
#include <gtest/gtest.h>

TEST(Set, DefaultConstSize)
{
    cd03::Set<int> set;
    EXPECT_EQ(set.size(),0);
}

TEST(Set, InsertElement)
{
    cd03::Set<int> set;
    set.insert(5);
    EXPECT_EQ(set.size(),1);
    set.insert(7);
    EXPECT_EQ(set.size(),2);
}
TEST(Set, RangeConstruct)
{
    int a = 20;
    std::vector<int> v1;
    while(0<a) { 
        v1.push_back(a); 
        --a;
    }
    cd03::Set<int> set(v1.begin(), v1.end());
    EXPECT_EQ(set.size(),20);

}
TEST(Set, PrintSet)
{
    int a = 1;
    std::vector<int> v1;
    while(a <= 20) {
        v1.push_back(a);
        ++a;
    }
    cd03::Set<int> set(v1.begin(), v1.end());
    EXPECT_EQ(set.size(),20);
/*    size_t s = 0;
    std::cout << *set.Root() << std::endl;
    set.printSet(set.Root(), s);*/
}
TEST(Set, EeaseValue) 
{
    int a = 1;
    std::vector<int> v1;
    while(a <= 20) {
        v1.push_back(a);
        ++a;
    }
    cd03::Set<int> set(v1.begin(), v1.end());
    std::cout << set.height(set.getRoot()) << std::endl;
    EXPECT_EQ(set.size(),20);
    set.erase(10);
    size_t s = 0;
    set.printSet(set.Root(), s);
    set.erase(16);
    size_t k = 0;
    set.printSet(set.Root(), k);
}

TEST(Set, Iterator)
{
    typedef std::vector<int> V;
    typedef cd03::Set<int> S;
    typedef S::iterator SIT;
    int a = 0;
    V v1;
    while(a <= 20) {
        v1.push_back(a);
        ++a;
    }
    S set(v1.begin(), v1.end()); 
    for(SIT i = set.begin(); i != set.end(); ++i) {
        std::cout << *i << " ";
    }
    std::cout << std::endl;
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

