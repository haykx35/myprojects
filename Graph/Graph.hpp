#ifndef __GRAPH_HPP__
#define __GRAPH_HPP__

#include <iostream>
#include <vector>
class Graph
{
public:
    struct Node {
        Node(int layer, int shape, int value)
            : layer_(layer)
            , shape_(shape)
            ,value_exists_(true)
            , value_(value)
            {}
        Node(int layer, int shape)
            : layer_(layer)
            , shape_(shape)
            , value_exists_(false)
            , value_()
            {}
    public:
        void addEdge(std::pair<int, int> edge);
        int getLayer() const;
        int getShape() const;
        int getValue() const;
        void setLayer(int layer);
        void setShape(int shape);
        bool valueExists() const;
        std::vector<std::pair<int, int> > edges_;        
    private:
        int layer_;
        int shape_;
        bool value_exists_;
        int value_;
    };
public:
    Node* getVertex(size_t i) const;
    Node* getVertex(int layer, int shape) const;
    void addVertex(int layer, int shape);
    void addVertex(int layer, int shape, int value);
    void addEdge(const int layer, const int shape, const int edgeLayer, const int edgeShape); 
    void addWithFile(); 
    void print() const;
    int algorithm();
    int minDistance(int dist[], bool sptSet[]);
    int find(int layer, int shape)const;
private:
    std::vector<Node*> vertex_; 
};
#endif /// __GRAPH_HPP__

