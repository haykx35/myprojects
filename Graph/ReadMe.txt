# Variable definition range

INT_MIN < layer < INT_MAX
INT_MIN < shape < INT_MAX
INT_MIN < edgeLayer < INT_MAX
INT_MIN < edgeShape < INT_MAX
INT_MIN <= value <= INT_MAX

Text file number including in /value/
Vertex layer including in /layer/
Vertex shape including in /shape/

If you want to include datas in file use addWithFile() function.
The algorithm by which your given problem is solved is call .algorithm() function.

###IMPORTANT###
Program reads the data only from "test1.input" file.
The data in the file must be written strictly in the order that you specified int the task.
Any deviation can lead to incorrect operation or failure of the output date.

Thanks for reading before launching problem.
