#ifndef __GRAPH_CPP__
#define __GRAPH_CPP__

#include "Graph.hpp"

#include <iostream>
#include <climits>
#include <sstream>
#include <fstream>

void 
Graph::addVertex(int layer, int shape)
{
    Node* temp = new Node(layer, shape);
    vertex_.push_back(temp);
}

void 
Graph::addVertex(int layer, int shape, int value)
{
    Node* temp = new Node(layer, shape, value);
    vertex_.push_back(temp);
}

Graph::Node*
Graph::getVertex(size_t i) const 
{
    return vertex_[i];
}

Graph::Node*
Graph::getVertex(int layer, int shape) const 
{
    for(size_t i = 0; i < vertex_.size(); ++i) {
        if(vertex_[i]->getLayer() == layer && vertex_[i]->getShape() == shape) {
            return vertex_[i];
        }
    }
    std::cout << "Error 1: Incorrect layer and shape" << std::endl;
    return NULL;
}

int
Graph::algorithm() {
    int min = INT_MAX;
    const size_t SIZE= vertex_.size();
    int graph[SIZE][SIZE];
    std::vector<int> vertex_with_value;
    
    for(size_t i = 0; i < SIZE; ++i) {
       if(vertex_[i]->valueExists())
            vertex_with_value.push_back(i);
       for(size_t j = 0; j < SIZE; ++j) {
            graph[i][j] = 0;
        }
    }

    for(size_t i = 0; i < SIZE; ++i) {
        for(size_t j = 0; j < vertex_[i]->edges_.size(); ++j) {
            graph[i][find(vertex_[i]->edges_[j].first,vertex_[i]->edges_[j].second)] = 1;
        }
    }
    for(size_t i = 0; i < vertex_with_value.size(); ++i) {
        int src = vertex_with_value[i] , dist[SIZE]; 
        bool sptSet[SIZE];	
        for (size_t i = 0; i < SIZE; i++)
            dist[i] = INT_MAX, sptSet[i] = false;

        dist[src] = 0;
        for (size_t count = 0; count < SIZE - 1; count++) {
            int u = minDistance(dist, sptSet);
            sptSet[u] = true;
            for (size_t v = 0; v < SIZE; v++) {
                if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
                    && dist[u] + graph[u][v] < dist[v])
                    dist[v] = dist[u] + graph[u][v];
            }
        }
        dist[src] = INT_MAX;
        for(size_t j = 0; j < vertex_with_value.size(); ++j) {
            if(dist[vertex_with_value[j]] < min && vertex_[vertex_with_value[i]]->getValue() != vertex_[vertex_with_value[j]]->getValue()) {
                min = dist[vertex_with_value[j]];
            }
    
        }
    }
    return min;
}

int 
Graph::find(int layer, int shape) const
{
    for(size_t i = 0; i < vertex_.size(); ++i) {
        if(vertex_[i]->getLayer() == layer && vertex_[i]->getShape() == shape)
            return i;
    }
    std::cerr << "Error 3: Cannot find vetex. Return INT_MIN" << std::endl;
    return INT_MIN;
}
int 
Graph::minDistance(int dist[], bool sptSet[])
{
	int min = INT_MAX, min_index;
	for (size_t v = 0; v < vertex_.size(); v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

void
Graph::addWithFile()
{
    int layer, shape, edgeLayer, edgeShape, value;
    char t;
    const char* filename = "test1.input";
    std::ifstream file;
    std::string str;
    file.open(filename);
    while(file) {
        std::getline(file,str,'\n');
        std::stringstream stream;
        if(str[0] == ' ' && str[1] == ' ' && str[2] == ' ' && str[3] == ' ') {
            stream << str;
            stream >> edgeLayer;
            stream >> edgeShape;
            addEdge(layer, shape, edgeLayer, edgeShape);
        }else if(str.length() > 1){
                stream << str;
                stream >> layer;
                stream >> shape;
                stream >> t;
                if(t == 'T') {
                    stream >> value;
                    addVertex(layer, shape, value);
                    t = 'U';
                } else {
                    addVertex(layer, shape);
                }
        }
        stream.clear();
    }
    file.close();
}

void 
Graph::print() const
{
    for (size_t i = 0; i < vertex_.size(); ++i) {
        std::cout << '(' << vertex_[i]->getLayer() 
                  << ',' << vertex_[i]->getShape() << ')'; 
        if(vertex_[i]->valueExists()) {
            std::cout << '(' << vertex_[i]->getValue() << ')';
        } 
        std::cout << " || "; 
        for (size_t j = 0; j < vertex_[i]->edges_.size(); ++j) {
            std::cout << '(' << vertex_[i]->edges_[j].first << ',' 
                      << vertex_[i]->edges_[j].second << ')';
            if(j+1 < vertex_[i]->edges_.size()) {
                std::cout << "->";
            }
        }
        std::cout << std::endl;
    }
}

void
Graph::addEdge(const int layer, const int shape, const int edgeLayer, const int edgeShape)
{
    std::pair<int, int> edge = std::make_pair(edgeLayer, edgeShape);
    for(size_t i = 0; i < vertex_.size(); ++i) {
        if(vertex_[i]->getLayer() == layer && vertex_[i]->getShape() == shape) {
            vertex_[i]->addEdge(edge);
            return;
        }
    }
}
void
Graph::Node::addEdge(std::pair<int, int> edge)
{
    edges_.push_back(edge);
}

int 
Graph::Node::getLayer() const
{
    return layer_;
}

int
Graph::Node::getShape() const
{
    return shape_;
}

void
Graph::Node::setLayer(int layer)
{
    layer_ = layer;
}

int 
Graph::Node::getValue() const
{
    if(valueExists()) {
        return value_;
    }
    return INT_MAX;
}

bool 
Graph::Node::valueExists() const
{
    return value_exists_;
}


void
Graph::Node::setShape(int shape)
{
    shape_ = shape;
}

#endif /// __GRAPH_CPP__

