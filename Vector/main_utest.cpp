#include "headers/Vector.hpp"

#include <gtest/gtest.h>

TEST(Vector, DefaultConstSize)
{
    cd03::Vector<int> v;
    EXPECT_EQ(v.size(), 0);
}

TEST(Vector, CopyConstructor)
{
    cd03::Vector<int> v1(3, 1);
    cd03::Vector<int> v2(v1);
    cd03::Vector<int> v3 = v1;
    cd03::Vector<int> v4;
    v4 = v1;

    EXPECT_EQ(v1.size(), v2.size());
    EXPECT_EQ(v1.size(), v3.size());
    EXPECT_EQ(v1.size(), v4.size());

    for (size_t i = 0; i < v1.size(); ++i) {
        EXPECT_EQ(v1[i], v2[i]);
        EXPECT_EQ(v1[i], v3[i]);
        EXPECT_EQ(v1[i], v4[i]);
    }
}

TEST(Vector, InputOutput)
{
    cd03::Vector<int> v;
    std::stringstream s("{1 2 3}");
    s >> v;
    std::stringstream str;
    str << v;
    EXPECT_EQ(str.str(), "{ 1 2 3 }");
}

TEST(Vector, Reserve)
{
    cd03::Vector<int> v(3,1);
    v.reserve(8);
    EXPECT_EQ(v.capacity(), 8);
}

TEST(Vector, ResizeByDefault)
{
    cd03::Vector<int> v(10);
    v.resize(21);
    EXPECT_EQ(v.size(), 21);
    EXPECT_EQ (v.capacity(), 21);
}

TEST(Vector, ResizeByInitialValue)
{
    cd03::Vector<int> v(10,1);
    v.resize(21,1);
    EXPECT_EQ(v.size(), 21);
    EXPECT_EQ (v.capacity(), 21);
    for (size_t i = 0; i < v.size(); ++i) {
        EXPECT_EQ(v.at(i), 1);
    }
}

TEST(Vector, push_back) 
{
    cd03::Vector<int> v(10,1);
    v.reserve(30);
    v.push_back(4);
    EXPECT_EQ(v.capacity(),30);
    EXPECT_EQ(v.at(10),4);
}

TEST(Vector, Iterator)
{
    typedef cd03::Vector<int> V;
    typedef V::iterator It;

    V v(4);
    V v1(4, 4);
    int value = 1;
    for (It it = v.begin(); it != v.end(); ++it) {
        *it = value;
        EXPECT_EQ(*it, 1);
    }
    for (It it = v1.begin(); it != v1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.end() - 1; it >= v1.begin(); --it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.begin(); it == v1.end() + 1; it += 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.end() - 1; it >= v1.begin(); it -= 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.begin(); it < v1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = v1.begin();
    EXPECT_EQ(*++it, 4);
    EXPECT_EQ(*it, 4);
    EXPECT_EQ(++(*it), 5);
    EXPECT_EQ(*(it += 1), 4);
    EXPECT_EQ(*(it -= 1), 5);
    EXPECT_EQ(*it, 5);
    EXPECT_EQ(it[1], 4);
    ///EXPECT_EQ(it->, 3);
}

TEST(Vector, ConstIterator)
{
    typedef cd03::Vector<int> V;
    typedef V::const_iterator It;

    V v;
    V v1(4, 4);
    v.resize(1, 1);
    v.resize(2, 2);
    v.resize(3, 3);
    v.resize(4, 4);
    for (It it = v1.begin(); it != v1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.end() - 1; it >= v1.begin(); --it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.begin(); it == v1.end() + 1; it += 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.end() - 1; it >= v1.begin(); it -= 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = v1.begin(); it < v1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = v.begin();
    EXPECT_EQ(*it, 1);
    EXPECT_EQ(*++it, 2);
    EXPECT_EQ(*it, 2);
    EXPECT_EQ(*it++, 2);
    EXPECT_EQ(*(it += 1), 4);
    EXPECT_EQ(*(it -= 1), 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(it[1], 4);
    It it1 = v.begin();
    It it3 = v.begin();
    It it2 = v.begin() + 3;
    EXPECT_EQ(it1 <= it2, true);
    EXPECT_EQ(it1 == it3, true);
    EXPECT_EQ(it2 - it1, 3);
    ///EXPECT_EQ(it->, 3);
}

TEST(Vector, ReverseConstIterator)
{
    typedef cd03::Vector<int> V;
    typedef V::const_reverse_iterator It;
    const V c(4, 4);

    for (It it = c.rbegin(); it != c.rend(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rend() - 1; it >= c.rbegin(); --it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rbegin(); it == c.rend() + 1; it += 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rend() - 1; it >= c.rbegin(); it -= 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rbegin(); it < c.rend(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = c.rbegin();
    EXPECT_EQ(*++it, 4);
    EXPECT_EQ(it[2], 4);
}

TEST(Vector, ReverseIterator)
{
    typedef cd03::Vector<int> V;
    typedef V::reverse_iterator It;

   V c(4, 4);
    for (It it = c.rbegin(); it != c.rend(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rend() - 1; it >= c.rbegin(); --it) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rbegin(); it == c.rend() + 1; it += 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rend() - 1; it >= c.rbegin(); it -= 1) {
        EXPECT_EQ(*it, 4);
    }
    for (It it = c.rbegin(); it < c.rend(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = c.rbegin();
    EXPECT_EQ(*++it, 4);
    EXPECT_EQ(it[2], 4);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

