#ifndef __T_VECTOR_HPP_
#define __T_VECTOR_HPP_

#include <iostream>

namespace cd03 {

template <typename T> class Vector;
template <typename T> std::istream& operator>>(std::istream& in, Vector<T>& v);
template <typename T> std::ostream& operator<<(std::ostream& out, const Vector<T>& v);

template <typename T>
class Vector
{
public:
    typedef T value_type; /// Container
    typedef value_type& reference; /// Container
    typedef const value_type& const_reference; /// Container
    typedef value_type* pointer; /// Container
    typedef std::ptrdiff_t difference_type; /// Container /// Signed integer type
    typedef std::size_t size_type; /// Container /// Unsigned integer type
public:
    class const_iterator
    {
        friend Vector;
    public:
        const_iterator(); /// trivial iterator (default constructible)
        ~const_iterator(); /// trivial iterator (default destructible
        const_iterator(const const_iterator& rhv); /// trivial iterator (assignable)
        const_iterator& operator=(const const_iterator& rhv); /// trivial (assignable)
        bool operator==(const const_iterator& rhv); /// trivial (assignable)
        bool operator!=(const const_iterator& rhv); /// trivial (assignable)
        bool operator<(const const_iterator& rhv); /// trivial (assignable)
        bool operator>(const const_iterator& rhv); /// trivial (assignable)
        bool operator<=(const const_iterator& rhv); /// trivial (assignable)
        bool operator>=(const const_iterator& rhv); /// trivial (assignable)
        const_reference operator*() const; /// trivial iterator (dereferencable)
        const_iterator& operator+=(const size_type size);
        const_iterator& operator-=(const size_type size);
        const_iterator& operator+(const size_type value) const;
        difference_type operator-(const_iterator& rhv ) const;
        const_iterator& operator-(const size_type value) const;
        const_iterator& operator++();
        const_iterator& operator--();
        const_iterator operator++(int);
        const_iterator operator--(int);
        const_reference operator[](const size_type size) const; 
        const value_type* operator->() const; /// trivial iterator (dereferencable)
    private:
        void setPtr(pointer ptr);
        pointer getPtr() const;
        explicit const_iterator(pointer ptr); 
    private:
        pointer ptr_; /// pointer to the current element
    };
    class iterator : public const_iterator
    {
        friend Vector;
    public:
        iterator();
        iterator(const iterator& rhv); /// trivial (assignable)
        iterator& operator=(const iterator& rhv);
        reference operator*(); /// trivial iterator (dereferencable)
        iterator& operator++();
        iterator& operator--();
        iterator operator++(int);
        iterator operator--(int);
        iterator& operator+=(const size_type size);
        iterator& operator-=(const size_type size);
        iterator operator+(const size_type value) const;
        iterator operator-(const size_type value) const;
        reference operator[](const size_type size); 
        value_type* operator->(); /// trivial iterator (dereferencable)
    private:
        explicit iterator(pointer ptr); 
    };
    class const_reverse_iterator : public const_iterator
    {
        friend Vector;
    public:
        const_reverse_iterator();
        const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const_reverse_iterator& operator++();
        const_reverse_iterator& operator--();
        const_reverse_iterator operator++(int);
        const_reverse_iterator operator--(int);
        const_reverse_iterator operator-(const size_type size) const;
        const_reverse_iterator operator+(const size_type size) const;
        const_reference operator[](const size_type size) const;
        const_reference operator*();
    protected:
        explicit const_reverse_iterator(pointer ptr);
    private:
    };
    class reverse_iterator : public const_reverse_iterator
    {
        friend Vector;
    public:
        reverse_iterator();
        reverse_iterator& operator=(const reverse_iterator& rhv);
        reference operator*(); /// trivial iterator (dereferencable)
        reverse_iterator operator+(const size_type value) const;
        reverse_iterator operator-(const size_type value) const;
        reverse_iterator& operator++();
        reverse_iterator& operator--();
        reverse_iterator operator++(int);
        reverse_iterator operator--(int);
        reference operator[](const size_type value); 
        const value_type* operator->() const; /// trivial iterator (dereferencable)
 
    protected:
       explicit reverse_iterator(pointer ptr);
    };
public:
    Vector(const size_t size = 0);
    Vector(const size_t size, const T& initialValue);
    Vector(const Vector<T>& rhv);
    ~Vector();
    const Vector<T>& operator=(const Vector<T>& rhv);
    size_t size() const;
    void resize(const size_t size);
    void resize(const size_t size, const T& initialValue);
    const T& at(const size_t index) const;
    T& at(const size_t index);
    const T& operator[](const size_t index) const;
    T& operator[](const size_t index);
    size_t capacity() const;
    void reserve(const size_t n);
    void push_back(const T& value); 
    const_iterator begin() const; 
    const_iterator end() const; 
    iterator begin();
    iterator end();
    const_reverse_iterator rbegin() const; 
    const_reverse_iterator rend() const; 
    reverse_iterator rbegin();
    reverse_iterator rend();

private:
    T* begin_;
    T* end_;
    T* bufferEnd_;

};

} /// namespace cd03

#include <sources/Vector.cpp>

#endif /// __T_VECTOR_HPP_

