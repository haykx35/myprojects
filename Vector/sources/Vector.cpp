#ifndef __T_VECTOR_CPP__
#define __T_VECTOR_CPP__

#include "headers/Vector.hpp"

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>

namespace cd03 {

static const double RESERVE_COEFF = 2;

template <typename T>
std::istream&
operator>>(std::istream& in, Vector<T>& v)
{
    char s;
    in >> s;
    if (s != '{') {
        std::cerr << "Error 1: Invalid symbol" << std::endl;
        ::exit(1);
    }
    char buffer[80];
    in.getline(buffer, 80, '}');
    size_t count = 0;
    for (size_t i = 0; i < ::strlen(buffer); ++i) {
        if (' ' == buffer[i]) {
            ++count;
        }
    }
    v.resize(count + 1);
    size_t i = 0;
    for (char* lex = ::strtok(buffer, " "); lex != NULL; lex = ::strtok(NULL, " ")) {
        std::stringstream s;
        s << lex;
        s >> v.at(i++);
    }
    /*in >> s;
    if (s != '}') {
        std::cerr << "Error 1: Invalid symbol" << std::endl;
        ::exit(1);
    }*/
    return in;
}

template <typename T>
std::ostream&
operator<<(std::ostream& out, const Vector<T>& v)
{
    out << "{ ";
    for (size_t i = 0; i < v.size(); ++i) {
        out << v.at(i) << ' ';
    }
    out << "}";
    return out;
}

template <typename T>
Vector<T>::Vector(const size_t size)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
   resize(size); 
}

template <typename T>
Vector<T>::Vector(const size_t size, const T& initialValue)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
   resize(size, initialValue); 
}

template <typename T>
Vector<T>::Vector(const Vector<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    resize(rhv.size());
    for (T* ptr = begin_, *sPtr = rhv.begin_; ptr != end_; ++ptr, ++sPtr) {
        *ptr = *sPtr;
    }
}

template <typename T>
Vector<T>::~Vector()
{
    if (begin_ != NULL) {
        ::free(begin_);
        begin_ = end_ = NULL;
    }
}

template <typename T>
const Vector<T>&
Vector<T>::operator=(const Vector<T>& rhv)
{
    if (rhv.begin_ == begin_) {
        return *this;
    }
    resize(rhv.size()); 
    for (T* ptr = begin_, *sPtr = rhv.begin_; ptr != end_; ++ptr, ++sPtr) {
        *ptr = *sPtr;
    }
    return *this;
}

template <typename T>
size_t
Vector<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
size_t
Vector<T>::capacity() const
{
    return bufferEnd_ - begin_;
}

template <typename T>
void
Vector<T>::resize(const size_t s)
{ 
    if (s > capacity()) {
        reserve(s);
    }
    const size_t oldSize = size();
     for (size_t i = oldSize; i < s; ++i) {
        begin_[i] = T();
    }
    for (size_t i = s; i < size(); ++i) {
        begin_[i].~T();
    }
    end_ = begin_ + s;
}

template <typename T>
void
Vector<T>::resize(const size_t s, const T& initialValue)
{
    if (s > capacity()) {
        reserve(s);
    }
    const size_t oldSize = size();
    for (size_t i = oldSize; i < s; ++i) {
        begin_[i] = initialValue;
    }
    for (size_t i = s; i < oldSize; ++i) {
        begin_[i].~T();
    }
    end_ = begin_ + s;
}

template <typename T>
void 
Vector<T>::reserve(const size_t s)
{
    if (capacity() < s) {
        T* temp = reinterpret_cast<T*>(::malloc(sizeof(T) * s));
        size_t i = 0;
        for (; i < size() && capacity() < s; ++i) {
            temp[i] = begin_[i];
        }
        if (begin_ != NULL) {
            ::free(begin_);
        }
        begin_ = temp;
        end_ = begin_ + i;
        bufferEnd_ = begin_ + s;
    }
    return;
}

template <typename T>
void
Vector<T>::push_back(const T& value)
{
    const size_t oldSize = size();
    const size_t oldCapacity = capacity();
    if (oldCapacity == oldSize) {
        reserve(0 == oldSize ? 1 : oldCapacity * RESERVE_COEFF);
    }
    resize(size() + 1, value);
}
template <typename T>
const T&
Vector<T>::at(const size_t index) const
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
T&
Vector<T>::at(const size_t index)
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
const T&
Vector<T>::operator[](const size_t index) const
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
T&
Vector<T>::operator[](const size_t index)
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

//////////////ITERATOR/////////
//////////////CONST_ITERATOR/////////
template <typename T>
Vector<T>::const_iterator::const_iterator(T* ptr)
   : ptr_(ptr)
{
}

template <typename T>
Vector<T>::const_iterator::const_iterator(const const_iterator& rhv)
   : ptr_(rhv.ptr_)
{
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
void
Vector<T>::const_iterator::setPtr(pointer ptr)
{
    ptr_ = ptr;
}
template <typename T>
bool
Vector<T>::const_iterator::operator!=(const const_iterator& rhv)
{
    return (ptr_ != rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator==(const const_iterator& rhv)
{
    return(ptr_ == rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator>=(const const_iterator& rhv)
{
    return(ptr_ >= rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator<=(const const_iterator& rhv)
{
    return(ptr_ <= rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator<(const const_iterator& rhv)
{
    return(ptr_ < rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator>(const const_iterator& rhv)
{
    return(ptr_ > rhv.ptr_);
}

template <typename T>
const T& 
Vector<T>::const_iterator::operator*() const
{
    return *ptr_;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator++()
{
    ++ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator++(int)
{
    pointer temp = const_iterator::getPtr();
    iterator it(temp);
    ++*this;
    return it;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator--()
{
    --ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator--(int index)
{
    pointer temp = const_iterator::getPtr();
    iterator it(temp);
    --*this;
    return it;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator+=(const size_type size)
{
    ptr_ += size;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator-=(const size_type size)
{
    ptr_ -= size;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator+(const size_type value) const
{
    ptr_ += value;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator-(const size_type value) const 
{
    ptr_ -= value;
    return *this;
}

template <typename T>
typename Vector<T>::difference_type
Vector<T>::const_iterator::operator-(const_iterator& rhv) const 
{
    return ptr_ - rhv.ptr_;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::end() const
{
    return const_iterator(end_);
}

template <typename T>
Vector<T>::const_iterator::~const_iterator()
{
   ptr_ = NULL;
}

template <typename T>
Vector<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator=(const const_iterator& rhv)
{
    ptr_ = rhv.ptr_;    
    return *this;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_iterator::operator[](const size_type size) const
{
    return *(ptr_ + size);
}
//////////////CONST_ITERATOR/////////
/////////////ITERATOR/////////

template<typename T>
typename Vector<T>::iterator
Vector<T>::begin()
{
    return iterator(begin_);
}

template<typename T>
typename Vector<T>::iterator
Vector<T>::end()
{
    return iterator(end_);
}

template <typename T>
Vector<T>::iterator::iterator(T* ptr)
   : const_iterator(ptr)
{
}

template <typename T>
Vector<T>::iterator::iterator(const iterator& rhv) 
 : const_iterator(rhv.ptr_)
{
}

template <typename T>
typename Vector<T>::reference
Vector<T>::iterator::operator*()
{
    const pointer ptr = const_iterator::getPtr();
    return *ptr;

}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this; 
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator++(int)
{
    pointer temp = const_iterator::getPtr();
    iterator it(temp);
    ++*this;
    return it;
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator--()
{
    const_iterator::setPtr(const_iterator::getPtr() - 1);
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator--(int index)
{
    pointer temp = const_iterator::getPtr();
    iterator it(temp);
    --*this;
    return it;
}
template <typename T>
Vector<T>::iterator::iterator()
 : const_iterator()
{
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator+=(const size_type size)
{
    const_iterator::setPtr(const_iterator::getPtr() + size);
    return *this;
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator-=(const size_type size)
{
    const_iterator::setPtr(const_iterator::getPtr() - size);
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator+(const size_type value) const
{
   return iterator(const_iterator::getPtr() + value);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator-(const size_type value) const 
{
   return iterator(const_iterator::getPtr() - value);
}

template <typename T>
typename Vector<T>::reference
Vector<T>::iterator::operator[](const size_type size)
{
    return *(const_iterator::getPtr() + size);
}
////ITERATOR////
///CONST_REVERSE_ITERATOR////
template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rbegin() const
{
    return const_reverse_iterator(end_- 1);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rend() const
{
    return const_reverse_iterator(begin_- 1);
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(pointer ptr)
    : const_iterator(ptr)
{
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator*()
{
    return *const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() - 1);
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator--()
{
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this;
}
template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator()
    : const_iterator()
{
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+(const size_type size) const
{
    return const_reverse_iterator(const_iterator::getPtr() - size);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-(const size_type size) const
{
    return const_reverse_iterator(const_iterator::getPtr() + size);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator[](const size_type size) const
{
    return *(const_iterator::getPtr() - size);
}

///CONST_REVERSE_ITERATOR////
///REVERSE_ITERATOR////
template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rbegin()
{
    return reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rend()
{
    return reverse_iterator(begin_ - 1);
}
template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(pointer ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename T>
typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator++()
{
    const_iterator::setPtr((const_iterator::getPtr() - 1));
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator--()
{
    const_iterator::setPtr((const_iterator::getPtr() + 1));
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator++(int)
{
    pointer temp = (const_iterator::getPtr());
    reverse_iterator it(temp);
    --*this;
    return it;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator--(int)
{
    pointer temp = (const_iterator::getPtr());
    reverse_iterator it(temp);
    ++*this;
    return it;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator-(const size_type value) const
{
    return   reverse_iterator((const_iterator::getPtr() + value));
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator+(const size_type value) const
{
    return   reverse_iterator((const_iterator::getPtr() - value));
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator*()
{
    return *(const_iterator::getPtr());
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator[](const size_type value)
{
    return *(const_iterator::getPtr() - value);
}

///REVERSE_ITERATOR////

} /// namespace cd03
#endif /// __T_VECTOR_CPP__

