#include "headers/Queue.hpp"
#include <list>
#include <gtest/gtest.h>

TEST(Vector, DefaultConstSize)
{
    cd03::Queue<int> queue1;
    EXPECT_EQ(queue1.size(), 0);
}

TEST(Vector, Push_Pop_Top)
{
    cd03::Queue<int> q2;
    EXPECT_EQ(q2.empty(), true);

    cd03::Queue<int> q1;
    q1.push(1);
    q1.push(2);
    q1.push(3);
    q1.push(4);
    q1.push(5);
    EXPECT_EQ(q1.size(), 5);
    EXPECT_EQ(q1.front(), 1);
    EXPECT_EQ(q1.back(), 5);
    q1.pop();
    EXPECT_EQ(q1.size(), 4);
    q1.pop();
    EXPECT_EQ(q1.front(), 3);
    EXPECT_EQ(q1.back(), 5);
    q1.pop();
    EXPECT_EQ(q1.size(), 2);
    EXPECT_EQ(q1.empty(), false);
    EXPECT_EQ(q1.front(), 4);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

