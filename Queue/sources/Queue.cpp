#ifndef __QUEUE_CPP__
#define __QUEUE_CPP__
#include "headers/Queue.hpp"
#include <iostream>
#include <cassert>

namespace cd03 {
template <typename T>
bool
operator==(const Queue<T>& lhv, const Queue<T>& rhv)
{
    return lhv.buffer_ == rhv.buffer_;
}

template <typename T>
bool
operator<(const Queue<T>& lhv, const Queue<T>& rhv)
{
    return lhv.buffer_ < rhv.buffer_;
}


template <typename T>
Queue<T>::Queue()
    : buffer_()
{
}

template <typename T>
Queue<T>::Queue(const Queue& rhv)
    : buffer_(rhv.buffer_)
{
}

template <typename T>
const Queue<T>&
Queue<T>::operator=(const Queue& rhv)
{
    if (&rhv == this) {
        return *this;
    }
    buffer_ = rhv.buffer_;
    return *this;
}

template <typename T>
bool
Queue<T>::empty() const
{
    return buffer_.empty();
}

template <typename T>
typename Queue<T>::size_type
Queue<T>::size() const
{
    return buffer_.size();
}

template <typename T>
typename Queue<T>::reference
Queue<T>::front()
{
    assert(!empty());
    return buffer_.front();
}

template <typename T>
typename Queue<T>::const_reference
Queue<T>::front() const
{
    assert(!empty());
    return buffer_.front();
}

template <typename T>
typename Queue<T>::reference
Queue<T>::back()
{
    assert(!empty());
    return buffer_.back();
}

template <typename T>
typename Queue<T>::const_reference
Queue<T>::back() const
{
    assert(!empty());
    return buffer_.back();
}

template <typename T>
void
Queue<T>::push(const value_type& value)
{
    buffer_.push_back(value);
}

template <typename T>
void
Queue<T>::pop()
{
    assert(!empty());
    buffer_.pop_front();
}
} ///namespace cd03
#endif ///__Queue_CPP__

