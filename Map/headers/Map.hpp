#ifndef __MAP_HPP__
#define __MAP_HPP__
#include <iostream>
#include <utility>

namespace cd03 {
template <typename Key, typename Data>
class Map
{
public:
    /// Container
    typedef std::pair<Key, Data> value_type;
    typedef Key key_type;
    typedef Data data_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;
private:
    struct Node {
        Node(const Key& k, const Data& d,
             Node* p = NULL,
             Node* l = NULL,
             Node* r = NULL)
        : value(k,d) , parent_(p),
            left_(l), right_(r)
        {}
        value_type value;
        Node* parent_;
        Node* left_;
        Node* right_;
    };
public:
    class const_iterator { /// BidirectionalIterator
        friend Map;
    public:
        const_iterator(); /// trivial
        const_iterator(const const_iterator& rhv);
        const const_iterator& operator=(const const_iterator& rhv); /// trivial
        value_type& operator*() const;
        const value_type* operator->() const;
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        bool operator!() const;
        operator bool() const;
        
        const const_iterator& operator++();
        const const_iterator& operator--();
        Node* nextInOrder(Node* ptr);
        Node* prevInOrder(Node* ptr);
        Node* getPtr() const;
        const value_type getValue() const;
        void setPtr(Node* ptr);
        const_iterator parent() const;
        const_iterator right() const;
        const_iterator left() const;
        const_iterator firstLeftParent();
        const_iterator firstRightParent();
        const_iterator& goParent();
        const_iterator& goRight();
        const_iterator& goLeft();
        void setParent(const_iterator& it);
        void setLeft(const_iterator& it);
        void setRight(const_iterator& it);
    protected:
        explicit const_iterator(Map::Node* ptr); 
        bool isLeftParent() const;
        bool isRightParent() const;
        bool isRoot() const;
        void createLeft(const value_type& x);
        void createRight(const value_type& x);
    private:
        Map::Node* ptr_;
    };
    class iterator : public const_iterator {
        friend Map;
    public:
        iterator();
        iterator(const iterator& rhv);
        iterator& operator=(const iterator& rhv);
        iterator& operator=(const const_iterator& rhv);
        value_type& operator*();
        iterator& goParent();
        iterator& goRight();
        iterator& goLeft();
        iterator parent() const;
        iterator right() const;
        iterator left() const;

    protected:
        explicit iterator(Node* ptr);
    };
public:
    /// Container
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
/*    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;*/
public:
    Map();
    Map(const Map& rhv); /// Container
    template <typename InputIterator>
    Map(InputIterator f, InputIterator l); /// Sequence
    ~Map();
    const Map& operator=(const Map&);
    std::pair<iterator, bool>
    insert(iterator& it, const value_type& x);
    std::pair<iterator, bool>
    insert(const value_type& value);
    void swap(Map&);
    size_type size() const; /// Container
    size_type max_size() const; /// Container
    bool empty() const; /// Container
    void clear();
    Node* leftMost(Node* n);
    Node* rightMost(Node* n);
    void printMap() const;
    void printMap(iterator it, size_type s) const;
    Node* getRoot() const;
protected:
    void rotateLeft(iterator& it);
    void rotateRight(iterator& it);
    int balance(Node* ptr) const;
    int height(Node* n) const;
    void balance(iterator& it);
    void goUp(iterator& it, const_reference x);
    bool goDown(iterator& it, const_reference x);
    void postOrderSize(Node* node,const size_type& size) const; 
private:
    Node* root_;
};

} /// namespace cd03
#include "sources/Map.cpp"
#endif /// __MAP_HPP__
