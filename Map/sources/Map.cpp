#ifndef __MAP_CPP__
#define __MAP_CPP__
#include "headers/Map.hpp"
#include <iostream>
#include <cassert>
#include <utility>

namespace cd03 {

template <typename Key, typename Data>
Map<Key, Data>::Map()
    : root_(NULL)
{
}

template <typename Key, typename Data>
Map<Key, Data>::~Map()
{
    if(NULL != root_) {
        clear();
    }
}

template <typename Key, typename Data>
template <typename InputIterator>
Map<Key, Data>::Map(InputIterator f, InputIterator l)
    : root_(NULL)
{
    while (f!=l) {
        insert(*f);
        ++f;
    }
}

template <typename Key, typename Data>
std::pair<typename Map<Key, Data>::iterator, bool>
Map<Key, Data>::insert(const value_type& x) 
{
    Node* ptr = root_;
    iterator it(ptr);
    return insert(it, x);
}

template <typename Key, typename Data>
typename Map<Key, Data>::Node*
Map<Key, Data>::getRoot() const
{
    return root_;
}

template <typename Key, typename Data>
std::pair<typename Map<Key, Data>::iterator, bool>
Map<Key, Data>::insert(iterator& it, const value_type& x)
{
    std::pair<iterator, bool> pair1;
    if(!it) {
        it.ptr_ = new Node(x.first, x.second); 
        if (NULL == root_) {
        root_ = it.getPtr();
        }
        pair1 = std::make_pair(it,true);
        return pair1; 
    }
    goUp(it, x);
    const bool isAdded = goDown(it, x);
    balance(it);
    pair1 = std::make_pair(it, isAdded);
    return pair1; 
}

template <typename Key, typename Data>
void 
Map<Key, Data>::goUp(Map::iterator& it, const_reference x) 
{
    if (!it || !it.parent() || *it == x ) {
        return;
    }
    const const_iterator temp =
        (x < *it) ? it.firstLeftParent()
                  : it.firstRightParent();
    if (temp.isRoot()) { return; }
    const bool isRightPlace =
        (x < *it) ? (*temp > x) : (*temp < x);
    if (isRightPlace) { return; }
    goUp(it = temp, x);
}

template <typename Key, typename Data>
bool
Map<Key, Data>::goDown(Map::iterator& it, const_reference x) {
    if (x < *it) {
        if (!it.left()) { 
            it.createLeft(x); return true;
        }
        return goDown(it.goLeft(), x);
    }
    if (x > *it) {
        if (!it.right()) {
            it.createRight(x); return true;
        }
        return goDown(it.goRight(), x);
    }
    return false;
}

template <typename Key, typename Data>
void
Map<Key, Data>::clear()
{
}

template <typename Key, typename Data>
size_t
Map<Key, Data>::size() const
{
    size_t size = 0;
    if(!empty()) {
        postOrderSize(root_, size); 
    }
    return size;

}
template <typename Key, typename Data>
void
Map<Key, Data>::printMap() const
{
    Map<Key, Data>::iterator it(root_);
    printMap(it, 0);
}

template <typename Key, typename Data>
void
Map<Key, Data>::printMap(iterator it, size_t size) const
{
    if (!it) {
        return;
    }
    printMap(it.right(), size + 1);
    for (size_t i = 0; i < size; ++i) {
        std::cout << "    ";
    }
    std::cout << '(' << it.getValue().first << '-' 
              << it.getValue().second << ") " << "(root-";
       if (it.parent()) {
        std::cout << '(' << it.parent().getValue().first
                  << it.parent().getValue().second << ')'; 
    } 
    std::cout << ')' << std::endl;
    printMap(it.left(), size + 1);
}

template <typename Key, typename Data>
void
Map<Key, Data>::postOrderSize(Node* n, const size_t& size) const
{
    if (NULL == n) {
        return;
    }
    postOrderSize(n->left_, size + 1);
    postOrderSize(n->right_, size + 1);
}

template <typename Key, typename Data>
bool
Map<Key, Data>::empty() const
{
    return root_ == NULL;
}

template <typename Key, typename Data>
typename Map<Key, Data>::Node*
Map<Key, Data>::leftMost(Node* n)
{
    assert(NULL != n);
    Node* temp = n;
    while (temp-> left_ != 0) {
        temp = temp->left_;
    }
    return temp;
}

template <typename Key, typename Data>
typename Map<Key, Data>::Node*
Map<Key, Data>::rightMost(Node* n)
{
    assert(NULL != n);
    Node* temp = n;
    while (temp-> right_ != 0) {
        temp = temp->right_;
    }
    return temp;
}

template <typename Key, typename Data>
void 
Map<Key, Data>::balance(iterator& it) 
{
    if (!it) { return; }
    iterator tempLeft = it.left();
    iterator tempRight = it.right();
    const int factor = balance(it.ptr_); /// 2 - 0
    if (factor > 1) {
        if (balance(tempLeft.ptr_) < 0) { /// 0 - 1
            rotateLeft(tempLeft);
        }
        rotateRight(it);
    } else if (factor < -1) {
        if (balance(tempRight.ptr_) > 0) {
            rotateRight(tempRight);
        }
        rotateLeft(it);
        } 
     balance(it.goParent()); 
}

template <typename Key, typename Data>
int
Map<Key, Data>::balance(Node* ptr) const 
{
   return height(ptr -> left_) - height(ptr -> right_); 
}

template <typename Key, typename Data>
int
Map<Key, Data>::height(Node* n) const
{
    if (NULL == n) { return -1;}
    return std::max(height(n->left_), height(n->right_)) + 1;
}

template <typename Key, typename Data>
void 
Map<Key, Data>::rotateLeft(iterator& it)
{
    iterator itParent = it.parent();
    iterator itRight = it.right();
    iterator itRightLeft = itRight.left();
    const bool isRightParent = it.isRightParent();
    it.setRight(itRightLeft);
    if (itRightLeft) { itRight.left().setParent(it); }
    itRight.setLeft(it);
    it.setParent(itRight);
    itRight.setParent(itParent);
    if (itParent) {
        isRightParent ? itParent.setLeft(itRight)
                      : itParent.setRight(itRight);
    } else { root_ = itRight.getPtr(); }
    it = itRight;
}

template <typename Key, typename Data>
void 
Map<Key, Data>::rotateRight(iterator& it) 
{
    iterator itParent = it.parent(), itLeft = it.left();
    iterator itLeftRight = itLeft.right();
    const bool isLeftParent = it.isLeftParent();
    it.setLeft(itLeftRight);
    if (itLeft.right()) { itLeft.right().setParent(it); }
    itLeft.setRight(it);
    it.setParent(itLeft);
    itLeft.setParent(itParent);
    if (itParent) {
        isLeftParent ? itParent.setRight(itLeft)
                     : itParent.setLeft(itLeft);
    } else { root_ = itLeft.getPtr(); }
    it = itLeft;
}

///CONST_ITERATOR///
template <typename Key, typename Data>
Map<Key, Data>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename Key, typename Data>
Map<Key, Data>::const_iterator::const_iterator(Node* node)
    :ptr_(node)
{
}

template <typename Key, typename Data>
Map<Key, Data>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}
template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator
Map<Key, Data>::begin() const
{
    if (NULL == root_) {
        return iterator(NULL);
    }
    return const_iterator(leftMost());
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator 
Map<Key, Data>::end() const
{
    return const_iterator(NULL);
}

template <typename Key, typename Data>
typename Map<Key, Data>::value_type&
Map<Key, Data>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_ -> value;
}

template <typename Key, typename Data>
const typename Map<Key, Data>::value_type
Map<Key, Data>::const_iterator::getValue() const
{
    return ptr_->value;
}

template <typename Key, typename Data>
bool
Map<Key, Data>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}

template <typename Key, typename Data>
bool
Map<Key, Data>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename Key, typename Data>
const typename Map<Key, Data>::const_iterator&
Map<Key, Data>::const_iterator::operator++()
{
    ptr_ = nextInOrder(ptr_);
    return *this;
}

template <typename Key, typename Data>
const typename Map<Key, Data>::const_iterator&
Map<Key, Data>::const_iterator::operator--()
{
    ptr_ = prevInOrder(ptr_);
}

template <typename Key, typename Data>
typename Map<Key, Data>::Node*
Map<Key, Data>::const_iterator::prevInOrder(Node* rhv) 
{    
    if (rhv->left_) {
        rhv = rhv->left_;
        while (rhv->rigth_ != NULL) {
            rhv = rhv->rigth_;
        }
        return rhv;
    }
    if (rhv->parent_ != NULL) {
        if(rhv->parent_->rigth_ == rhv) {
            rhv = rhv->parent_;
            return rhv;
        }
        while ((rhv->parent_) && rhv != rhv->parent_->rigth_) {
            rhv = rhv->parent_;
        }
        rhv = rhv->parent_;
        return rhv;
    }
    return rhv;
}

template <typename Key, typename Data>
typename Map<Key, Data>::Node*
Map<Key,Data>::const_iterator::nextInOrder(Node* rhv) 
{
    if (rhv->right_) {
        rhv = rhv->right_;
        while (rhv->left_ != NULL) {
            rhv = rhv->left_;
        }
        return rhv;
    }
    if (rhv->parent_ != NULL) {
        if (rhv->parent_->left_ == rhv) {
            rhv = rhv->parent_;
            return rhv;
        }
        while ((rhv->parent_) && rhv != rhv->parent_->left_) {
            rhv = rhv->parent_;
        }
        rhv = rhv->parent_;
        return rhv;
    }
    return rhv;
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator&
Map<Key, Data>::const_iterator::goLeft()
{
    ptr_ = ptr_ -> left_;
    return *this;
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator&
Map<Key, Data>::const_iterator::goRight()
{
    ptr_ = ptr_ -> right_;
    return *this;
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator&
Map<Key, Data>::const_iterator::goParent()
{
    ptr_ = ptr_ -> parent_;
    return *this;
}

template <typename Key, typename Data>
bool
Map<Key, Data>::const_iterator::operator!() const
{
    return (ptr_ == NULL);
}

template <typename Key, typename Data>
Map<Key, Data>::const_iterator::operator bool() const
{
    return (ptr_ != NULL);
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator
Map<Key, Data>::const_iterator::parent() const
{
    assert(ptr_ != NULL);
    return const_iterator(ptr_ -> parent_);
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator
Map<Key, Data>::const_iterator::left() const
{
    return const_iterator(ptr_ -> left_);
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator
Map<Key, Data>::const_iterator::right() const
{
    return const_iterator(ptr_ -> right_);
}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator
Map<Key, Data>::const_iterator::firstLeftParent()
{
    const_iterator temp(ptr_);
    while(!temp.isLeftParent()) {temp.goParent();} 
    temp.goParent();
    return temp;

}

template <typename Key, typename Data>
typename Map<Key, Data>::const_iterator
Map<Key, Data>::const_iterator::firstRightParent()
{
    const_iterator temp(ptr_);
    while(!temp.isRightParent()) { temp.goParent();}
    temp.goParent();
    return temp;
}

template <typename Key, typename Data>
void
Map<Key, Data>::const_iterator::setPtr(Node* ptr)
{
    ptr_ = ptr;
}

template <typename Key, typename Data>
typename Map<Key, Data>::Node*
Map<Key, Data>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename Key, typename Data>
bool
Map<Key, Data>::const_iterator::isLeftParent() const
{  
    if (ptr_ -> parent_ == NULL) {
        return false;
    }
    if (ptr_ -> parent_ -> right_ == ptr_ ) {
        return true;
    }
    return false;
}

template <typename Key, typename Data>
bool
Map<Key, Data>::const_iterator::isRightParent() const
{
    if (ptr_ -> parent_ == NULL) {
        return false;
    }
    if (ptr_ -> parent_ -> left_ == ptr_) {
        return true;
    }
    return false;
}

template <typename Key, typename Data>
bool
Map<Key, Data>::const_iterator::isRoot() const
{
    return (ptr_ -> parent_ == NULL);
}

template <typename Key, typename Data>
void
Map<Key, Data>::const_iterator::createLeft(Map::const_reference x)
{
    ptr_ -> left_ = new Node(x.first, x.second, ptr_, NULL, NULL);
    ptr_ = ptr_ -> left_;
}

template <typename Key, typename Data>
void
Map<Key, Data>::const_iterator::createRight(Map::const_reference x)
{
    ptr_ -> right_ = new Node(x.first, x.second, ptr_, NULL, NULL);
    ptr_ = ptr_ -> right_;
}

template <typename Key, typename Data>
void
Map<Key, Data>::const_iterator::setParent(const_iterator& it)
{
    ptr_ -> parent_ = it.ptr_;
}

template <typename Key, typename Data>
void
Map<Key, Data>::const_iterator::setLeft(const_iterator& it)
{
    ptr_ -> left_ = it.ptr_;
}

template <typename Key, typename Data>
void
Map<Key, Data>::const_iterator::setRight(const_iterator& it)
{
    ptr_ -> right_ = it.ptr_;
}


///CONST_ITERATOR///
///ITERATOR///
template <typename Key, typename Data>
Map<Key, Data>::iterator::iterator()
    : const_iterator()
{
}

template <typename Key, typename Data>
Map<Key, Data>::iterator::iterator(Node* node)
    : const_iterator(node)
{
}

template <typename Key, typename Data>
Map<Key, Data>::iterator::iterator(const iterator& rhv)
    :const_iterator(rhv)
{
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator
Map<Key, Data>::begin()
{
    if (NULL == root_) {
        return iterator(NULL);
    }
    return iterator(leftMost(root_));    
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator
Map<Key, Data>::end()
{
    return iterator(NULL);
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator&
Map<Key, Data>::iterator::operator=(const iterator& rhv)
{
    if(this == &rhv) {
        return *this;
    }
    const_iterator::setPtr(rhv.getPtr());
    return *this;
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator&
Map<Key,Data>::iterator::operator=(const const_iterator& rhv)
{
    if (&*this == &rhv) {
        return *this;
    }
    const_iterator::setPtr(rhv.ptr_);
    return *this;
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator&
Map<Key, Data>::iterator::goRight()
{
    const_iterator::setPtr(const_iterator::getPtr() -> right_);
    return *this;
}

template <typename Key, typename Data>
typename Map<Key,Data>::iterator&
Map<Key, Data>::iterator::goLeft()
{
    const_iterator::setPtr(const_iterator::getPtr() -> left_);
    return *this;
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator&
Map<Key, Data>::iterator::goParent()
{
    const_iterator::setPtr(const_iterator::getPtr() -> parent_);
    return *this;
}

template <typename Key, typename Data>
typename Map<Key, Data>::value_type&
Map<Key, Data>::iterator::operator*()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_ -> value;
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator
Map<Key, Data>::iterator::parent() const
{
    assert(const_iterator::ptr_ != NULL);
    return iterator(const_iterator::getPtr() -> parent_);
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator
Map<Key, Data>::iterator::left() const
{
    return iterator(const_iterator::ptr_ -> left_);
}

template <typename Key, typename Data>
typename Map<Key, Data>::iterator
Map<Key, Data>::iterator::right() const
{
    return iterator(const_iterator::ptr_ -> right_);
}

///ITERATOR///
}/// namespace cd03
#endif ///__MAP_CPP__
