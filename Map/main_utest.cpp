#include "headers/Map.hpp"
#include <gtest/gtest.h>
#include <iostream>
#include <vector>
#include <utility>

TEST(Map, DefaultConstSize)
{
    cd03::Map<char, int> map;
    EXPECT_EQ(map.size(),0);
}

TEST(Map, ConstIterator)
{
    typedef cd03::Map<char, int> M;
    typedef M::const_iterator MCI;
    M map;
    for (MCI it1 = map.begin(); it1 != map.end(); ++it1) {
    std::cout << (*it1).second  << " ";
    }
    std::cout << std::endl;
}

TEST(Map, Iterator)
{
}

TEST(Map, InsertRange)
{
    std::vector<std::pair<char, int> > v;
    v.push_back(std::make_pair('a', 1));
    v.push_back(std::make_pair('b', 2));
    v.push_back(std::make_pair('c', 3));
    v.push_back(std::make_pair('d', 4));
    cd03::Map<char, int> map(v.begin(), v.end());
}

TEST(Map, Erase)
{
    std::vector<std::pair<char, int> > v;
    char c = 'a';
    for(int i = 1; i < 10; ++i, ++c) {
        v.push_back(std::make_pair(c,i));
    }
    cd03::Map<char, int> map(v.begin(), v.end());
}

TEST(Map, Print)
{   
    std::vector<std::pair<char, int> > v;
    char c = 'a';
    for(int i = 1; i < 10; ++i, ++c) {
        v.push_back(std::make_pair(c,i));
    }
    cd03::Map<char, int> map(v.begin(), v.end());
    map.printMap();
}
int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

